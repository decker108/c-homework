#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

int antal,antal2;
long tab[35];

long fib(long n) {
    antal++;
    if (n == 0 || n == 1) {
        return 1;
    } else {
        return fib(n-1)+fib(n-2);
    }
}

int fib3 (int n) {
    int f1=1, f2=1, f3=1, i;
    for (i=3; i<=n; i++) {
    	f3 = f1+f2;
    	f1 = f2;
    	f2 = f3;
    }
    //return f3;
    return f1+f2;
}

long fibdyn(long n) {
    antal2++;
    if (tab[n] >= 0) {
        return tab[n];
    } else {
        long f1 = fibdyn(n-1);
        tab[n-1] = f1;
        long f2 = fibdyn(n-2);
        tab[n-2] = f2;
        return (f1+f2);
    }
}

int main(void) {
	long n = 33;
	tab[0] = 1;
	tab[1] = 1;
	int i;
	for (i=2; i<35; i++) {
		tab[i] = -1;
	}
	
	//printf("fib(%d) = %ld (antal=%d) \n",n,fib(n),antal);
	printf("fibdyn(%d) = %ld (antal=%d) \n",n,fibdyn(n),antal2);
	printf("fib3(%d) = %ld (antal=%d) \n",n,fib3(n),1);
	
	return 0;
}
