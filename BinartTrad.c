#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct objekt{
   char nyckel[10];
   struct objekt *left,*right;
};

typedef struct objekt objekt;

objekt *insert(objekt *start,objekt ny) {
    objekt *x,*y,*z;
    z=(objekt *) malloc(sizeof(objekt));
    *z = ny; //�verf�r inneh�llet i ny till z
    y = NULL;
    x = start;
    while(x!=NULL) {
        y = x;
        //eftersom det �r ett bin�rt s�ktr�d kommer namnen att sorteras med det
        //l�gre alfabetiska v�rdet till v�nster och det h�gre till h�ger.
        if (strcmp(z->nyckel,x->nyckel)<0) {
            x = x->left;
        } else {
            x = x->right;
        }
    }
    if(y==NULL) { //om tr�det �r tomt blir z start (rot)
        start = z;
    } else { //annars �r y den l�gsta noden
        if(strcmp(z->nyckel,y->nyckel)<0) { //placera z (ny nod) �t h�ger/v�nster
            y->left = z;
        } else {
            y->right = z;
        }
    }
    return start;
}


// Den h�r funktionen ska du skriva
void inorder(objekt *start){
    objekt *x;
    x = start;
    if(x != NULL){
        inorder(x->left);
        printf("%s ",x->nyckel);
        inorder(x->right);
    }
}

void postorder(objekt *start){
   objekt *x;
   x = start;
   if(x!=NULL){
      postorder(x->left);
      postorder(x->right);
      printf("%s ",x->nyckel);
   }
}

// Den h�r funktionen ska du skriva
void preorder(objekt *start){
    objekt *x;
    x = start;
    if(x != NULL){
        printf("%s ",x->nyckel);
        preorder(x->left);
        preorder(x->right);
    }
}

int main(void){
   objekt *start,ny;
   FILE *fil;
   int klar=0;
   char namn[10];

   start=NULL;
   fil=fopen("bintrad3.dat","rt");
   while(!klar){
      fscanf(fil,"%s",namn);
      if(!feof(fil)) {
         strcpy(ny.nyckel,namn);
         ny.left=NULL;
         ny.right=NULL;
         start=insert(start,ny);
      } else {
         klar = 1;
      }
   }
   fclose(fil);
   
   printf("Preorder:  ");
   preorder(start);
   printf("\n");
   
   printf("Inorder:   ");
   inorder(start);
   printf("\n");
   
   printf("Postorder: ");
   postorder(start);
   printf("\n");
   
   return 0;
}
