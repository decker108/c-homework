//Exempeltenta 1, Uppgift 4 Ordbehandling
#include <stdio.h>
#define LENGTH 10
int left(int cur){
    return --cur;
}
int right(int cur){
    return ++cur;
}
void kill(char w[],int cur){
    int i;
    for(i=cur;w[i]!='\0';i++)
    w[i]=w[i+1];
}
void change(char w[],char c,int cur){
    w[cur]=c;
}
void insert(char w[],char c,int cur){
    int n=0,i;
    while(w[n]!='\0')
        n++;
    for(i=n;i>=cur;i--)
        w[i+1]=w[i];
    w[cur]=c;
}
void drop(char w[],int cur){
    w[cur+1]='\0';
}
int main(void){
    int cur=0;
    char ord[LENGTH]="TENTAMEN";
    change(ord,'P',cur);
    cur=right(cur);
    cur=right(cur);
    insert(ord,'N',cur);
    cur=right(cur);
    cur=right(cur);
    kill(ord,cur);
    drop(ord,cur);
    printf("%s\n",ord);
}
