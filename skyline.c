#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

int data[8][4]={{0,30,15,0} ,{5,20,25,0} ,{10,40,30,0},
                {40,20,50,0},{45,10,75,0},{55,45,65,0},
                {70,40,95,0},{80,20,90}};

void skyline(int a[], int low, int high) {
    int a1[32],a2[32],i1,i2,i3,i,n,h1,h2;
    if (high-low == 0) {
        for (i=0; i<4; i++) {
        	a[i] = data[low][i];
        }
    } else {
        skyline(a1,low,(low+high)/2);
        skyline(a2,(low+high)/2+1,high);
        i1=0; i2=0; i3=0; h1=0; h2=0;
        n=2*(high-low+1)-1;
        while(i1<=n || i2<=n) {
            if(i1<=n && (a1[i1]<a2[i2] || i2>=n)){
                h1=a1[i1+1];
                a[i3++]=a1[i1++];
                if(h1>=h2) {
                    a[i3++]=a1[i1++];
                } else {
                    a[i3++]=h2;
                    i1++;
                }
            } else { 
                if(i2<=n && (a2[i2]<=a1[i1] || i1>=n)) {
                    h2=a2[i2+1];
                    a[i3++]=a2[i2++];
                    if(h2>=h1) {
                        a[i3++]=a2[i2++];
                    } else {
                        a[i3++]=h1;
                        i2++;
                    }
                }
            }
        }
    }
}

int main(void) {
	int i,j=0,a[32];
	skyline(a,0,7);
	
	printf("(%d,%d) ",a[0],a[1]);
	for (i=3; i<32; i+=2) {
		if (a[i] != a[i-2]) {
		    printf("(%d,%d) ",a[i-1],a[i]);
		}
	}
	
	return 0;
}
