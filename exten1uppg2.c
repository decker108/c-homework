//Exempeltenta 1, Uppgift 2 Statliga cyklar
#include <stdio.h>
int main(void){
    FILE *fil;
    int i,n,stad[10],nt,fran,till,antal;
    fil=fopen("cyklar.txt","rt");
    fscanf(fil,"%d",&n);
    for(i=0;i<n;i++) {
        fscanf(fil,"%d",&stad[i]);
    }
    fscanf(fil,"%d",&nt);
    for(i=0;i<nt;i++){
        fscanf(fil,"%d %d %d",&fran,&till,&antal);
        stad[fran-1]-=antal;
        stad[till-1]+=antal;
    }
    fclose(fil);
    for(i=0;i<n;i++) {
        printf("I stad %d finns %d\n",i+1,stad[i]);
    }
}
