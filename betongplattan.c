//CL050315 Uppg 5 Betongplattan
#include <stdio.h>
#include <stdlib.h>
int main(void) {
    //Variabler
    int antal_rader=0,antal_tecken=0,antal_storhal=0;

    //fill�sningskod
    FILE *fil;
    fil=fopen("betong.txt","rt");
    fscanf(fil,"%d", &antal_rader);
    fscanf(fil,"%d", &antal_tecken);
    if (!(3<=antal_rader && antal_rader<=10)) {
        printf("Fel antal rader!");
        return(-1);
    }
    if (!(10<=antal_tecken && antal_tecken<=40)) {
        printf("Fel antal tecken!");
        return(-1);
    }
    char betong[10][41];

    //l�s in filen till en str�ng
    for (int a=0; a<antal_rader; a++) {
    	fscanf(fil,"%s",betong[a]);
    }

    //loop som g�r igenom rad-f�r-rad, tecken-f�r-tecken tittar efter h�l
        //hittas ett h�l, titta efter h�l till h�ger, under och �ver
    for (int a=0; a<=antal_rader; a++) {
        for (int b=0; b<=antal_tecken; b++) {
            if (betong[a][b]=='o') {
                if (b<antal_tecken-1 && betong[a][b+1]=='o') { //Om det inte �r det sista tecknet
                    antal_storhal++;
                }
                if (a<antal_rader-1 && betong[a+1][b]=='o') { //Om det inte �r den sista raden
                    antal_storhal++;
                }
            }
        }
    }

    //skriv ut antalet h�l
        //st�ng filen
    printf("Det fanns %d h�l",antal_storhal);
    fclose(fil);
}
/*
R�tt svar: 10 stora h�l
*/
