#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int hemlig = 0;
int glob_antal = 0;

int gissa(int min, int max, int antal) {
    int x;
    x = (min+max)/2;
    if (x == hemlig) {
        //glob_antal = antal;
        return antal; //varf�r returnera antal?
    } else {
        if (x < hemlig) {
            return gissa(x+1,max,antal+1);
        } else {
            return gissa(min,x-1,antal+1);
        }
    }
}

int main(int argc, char* args[]) {
    printf("Skriv in hemligt tal: ");
    scanf("%d",&hemlig);
    printf("Antal: %d \n",gissa(1,100,0));
    //printf("Antal gissningar: %d",glob_antal);
    exit(0);
}
/*
local x
x <- min + max / 2
if x == hemlig
    then return antal
    else
        if x < hemlig
            then gissa(x+1,max,antal+1)
            else gissa(min,x-1,antal+1)
*/
