//CL060111 Laboration 1: T�rningshasard
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
int main(void) {
	//variabler
	int antal_sidor,antal_tarningar;
    srand(time(0));

	//Ange antal sidor t�rningarna har (max 12)
	do {
        printf("Ange antal sidor t�rningarna har (h�gst 12): ");
        scanf("%d",&antal_sidor);
	} while (antal_sidor>=13);

	//Ange antal t�rningar som sl�s (max 50)
    do {
        printf("Ange antal t�rningar som sl�s (h�gst 50): ");
        scanf("%d",&antal_tarningar);
	} while (antal_tarningar>=51);

    int bank_vinst_antal=0;

    for (int k=1; k<=10000; k++) {
        //skapa array med ett fack per t�rningssida (+1 f�r att t�rningsslag ej blir 0)
        int tarning[antal_sidor+1];
        //Nollst�ll array
        for (int i=0; i<=antal_sidor; i++) {
            tarning[i]=0;
        }
        //Sl� t�rningar, spara resultaten i array
        for (int i=1; i<=antal_tarningar; i++) {
            int kast = rand()%antal_sidor+1;
            tarning[kast]=1;
        }
        //Kontrollera om t�rningarna bildat en stege
        int bank_vinst=1; //Banken vinner by default
        for (int a=1; a<=antal_sidor; a++) { //stegens tal best�ms av antalet sidor
            if (tarning[a]==0) { //Om ett enda tal i t�rnings-arrayen saknas f�rlorar banken
                bank_vinst=0;
                break;
            }
        }
        //om det fanns en stege, �ka bankens antal vunna matcher med +1
        if (bank_vinst==1) {
            bank_vinst_antal++;
        }
    }
    printf("Banken vann %f%% av spelen.",bank_vinst_antal/10000.0*100);
    //yttre-loop som g�r 10000 varv
        //inre-loop som g�r antal_t�rningar varv (max 50)
            //OM alla slagna t�rningar bildar en stege => bank_vinst++
                //Sortera talen i en array i stigande ordning, kolla om avst�nden
                //mellan alla talen �r 1
            //ANNARS spelare_vinst++

    //Vinstprocent = bank_vinst/10000
}
