#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

float rot(float n, float a, float e){
    if (fabs(a*a-n) < e) {
        return a;
    } else {
        return rot(n,(a*a+n)/(2*a),e);
    }
}

int main(void) {
    float n = 0;
    float a = 1.0;
    float e = 0.001;
	while (1) {
		printf("Ange n: ");
		scanf("%f",&n);
		if (n >= 1.0 && n <= 300.0) {
		    break;
		}
	}
	printf("Roten ur n = %f",rot(n,a,e));
	return 0;
}