//Exempeltenta 1, Uppgift 6 Mätarställningen
#include <stdio.h>
int kolla(int hm,int tm){
    int s[10]={0},i,ok=1;
    if(hm<100000)
        s[0]++;
    while(hm>0){
        s[hm%10]++;
        hm=hm/10;
    }
    if(tm<1000)
    s[0]++;
    while(tm>0){
        s[tm%10]++;
        tm=tm/10;
    }
    for(i=0;i<10;i++){
        //printf("%d ",s[i]);
        if(s[i]!=1)
        ok=0;
    }
    return ok;
}
int main(void){
    int hm,tm,ok=0,km=0;
    printf("Huvudmätare : ");
    scanf("%d",&hm);
    printf("Trippmätare : ");
    scanf("%d",&tm);
    while(!ok){
        if (kolla(hm,tm)){
            printf("Efter %d km\n",km);
            ok=1;
        }
        else {
            tm++;
            if (tm==10000)
            tm=0;
            hm++;
            if (hm==1000000)
            hm=0;
        }
        km++;
    }
}
