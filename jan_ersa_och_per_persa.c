#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

int **graph;

void dijkstra(int roadCount,int nodeCount,int start,int stop,int path[]) {
    int t[nodeCount],i,j,v,x,min;
    for(i=0;i<nodeCount;i++) {
        path[i] = INT_MAX;
        t[i] = 1;
    }
    path[start] = 0;
    while(t[stop]) {
        min = INT_MAX;
        for(i=0;i<nodeCount;i++) {
            if(t[i] && path[i]<min) {
                v = i;
                min = path[i];
            }
        }
        t[v] = 0;
        for(j=0;j<roadCount;j++) {
            x = -1;
            if(graph[j][0]==v) {
                x = graph[j][1];
            }
            if(graph[j][1]==v) {
                x = graph[j][0];
            }
            if(x>=0 && path[x]>path[v]+graph[j][2]) {
                path[x] = path[v]+graph[j][2];
            }
        }
    }
}

int main(void) {
	int antal_hus,antal_vagar;
	FILE *fil;
	fil = fopen("karta.txt","rt");
	fscanf(fil,"%d",&antal_hus);
	fscanf(fil,"%d",&antal_vagar);
    
    int x;
    graph = malloc(sizeof(int *) * antal_vagar);
    for(x = 0; x < antal_vagar; x ++) {
        graph[x] = malloc(sizeof(int) * 3);
    }
    int *path;
    path = malloc(sizeof(int *) * antal_hus);
    
    int a,from,to,cost;
    for (a=0; a<antal_vagar; a++) {
    	fscanf(fil,"%d %d %d",&from,&to,&cost);
    	graph[a][0] = from-1;
    	graph[a][1] = to-1;
    	graph[a][2] = cost;
    } //l�sa in omv�nt?
    
    int minStart,minStop,minDist = 0,i,j;
    for (i=0; i<antal_hus; i++) {
        for (j=0; j<antal_hus; j++) {
            if (i == j) {
                continue;
            }
        	dijkstra(antal_vagar,antal_hus,i,j,path);
            if (path[j] > minDist) {
                minStart = i;
                minStop = j;
                minDist = path[j];
            }
        }
    }
    printf("Mellan hus %d och %d �r l�ngsta kortaste avst�ndet %d m",minStart+1,minStop+1,minDist*100);
	
	return 0;
}
