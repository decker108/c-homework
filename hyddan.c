//Hyddan CL040413 Uppgift 2
//Ola Rende, DP09
#include <stdio.h>
#include <math.h>
int main(void){
    float maxv=0,maxvr=0;
    float sumv;
    float p;
    float pi=3.141592654;
    printf("Ange pinnarnas l�ngd i hela meter: ");
    scanf("%f",&p);
    for (float r=0; r<=p; r=r+p/100) {
    	float cv=pi*r*r*p; //cylindervolym
    	float kv=(pi*r*r*sqrt(p*p-r*r))/3; //konvolym
    	sumv=cv+kv;
        if (sumv>maxv) {
            maxv=sumv;
            maxvr=r;
        }
    }
    printf("Pinnarnas l�ngd �r: %f \n",p);
    printf("Maximal volym �r: %2.2f m3 vid radien %2.2f m",maxv,maxvr);
}
