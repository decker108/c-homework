//CL071026 Uppg 1 Plattl�ggning
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
float frac(float f) {
    float fracf=f-(int)f;
    return fracf;
}
int main(void) {
	int antal_plattor;

    //L�sa in antalet plattor, allts� ursprungstalet.
	printf("Antal plattor: ");
	scanf("%d",&antal_plattor);

    int i=0; //i �r en variabel f�r stegvis r�kning.
    //loop som forts�tter tills det finns 0 plattor kvar.
    while (antal_plattor>0) {
        //i f�r samma v�rde som antal plattor, for-loopen g�r tills i=1, i=i-1 per varv.
        for (i=antal_plattor; i>0; i--) {
            //Om roten ur i g�r j�mnt, skriv i som svar.
            if (frac(sqrt((float)i))==0) { //s�nd kvadroten av i (i^0.5) till funken frac om det blir n�gra decimaler kvar efter roten ur.
                printf("%d ",i); //Skriv svaret p� kvadratroten.
                break; //hoppa ur for loopen (tv� klammerparenteser ned�t).
            }
        }
        //n�r en kvadratrot har g�tt ut j�mnt, minska ursprungstalet med kvadrotstalet.
        antal_plattor-=i;
    }
}
/*
Exempel-k�rning:

antal_plattor=26
i=26
roten_ur:26 funkar ej
i=25
roten_ur:25 funkar
svar: 25
antal_plattor=26-25=1
i=1
roten_ur:1 funkar
svar: 1
*/
