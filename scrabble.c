#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct bokstavstyp Bokstavstyp;

struct bokstavstyp {
    unsigned int antal;
    int tempantal;
    int poang;
};

void fixaantal (Bokstavstyp bokstav[256]) {
    int i;
    for (i=0; i<256; i++) {
        //flytta antal till tempantal
        if (bokstav[i].poang!=NULL) {
            bokstav[i].tempantal=bokstav[i].antal;
        }
    }
}

int beraknapoang (char testord[], Bokstavstyp bokstav[256]) {
    int langd = strlen(testord);
    int a,poang=0;
    //loopen g�r tecken f�r tecken igenom ordet och ber�knar po�ng, minskar tempantal.
    for (a=0; a<langd; a++) {
        unsigned int temptecken=(unsigned int)testord[a]; //flytta �ver testbokstaven till temptecken
        (bokstav[temptecken].tempantal)-=1; //varje bokstav som anv�nds dras ifr�n tempantalet i structen.
        if (temptecken>256) {
            return 0; //om bokstaven inte finns blir det noll po�ng
        }
        if ((bokstav[temptecken].tempantal)<0) {
            return 0; //om antalet bokst�ver tagit slut blir det noll po�ng
        }
        poang+=(bokstav[temptecken].poang); //varje bokstavspo�ng adderas till summan
    }
    return poang;
}

int main(void) {
    struct bokstavstyp bokstav[256];
    int totalpoang=0;
	FILE *bokstavsfil;
	//bokstavsfil=fopen("brickor.txt","rt");
	bokstavsfil=fopen("brickor2.txt","rt");

    //nollst�ll structen
    int i;
    for (i=0; i<256; i++) {
    	bokstav[i].antal=0;
    	bokstav[i].tempantal=0;
    	bokstav[i].poang=0;
    }

    //l�s in bokstavsdata i structen
	unsigned char temptecken;
	while (!feof(bokstavsfil)) { //loopen g�r till filslutet, sparar i structen.
        int antal,poang;
        fscanf(bokstavsfil,"%c %d %d",&temptecken,&antal,&poang);
        bokstav[temptecken].antal=antal;
        bokstav[temptecken].poang=poang;
        bokstav[temptecken].tempantal=bokstav[temptecken].antal;
	}
	fclose(bokstavsfil);

	//l�s in ett ord, dela upp, ber�kna totalpo�ng
    int antalord,b;
    char testord[20];
    char maxord[20];
    FILE *ordfil;
    ordfil=fopen("orden2.txt","rt");
    fscanf(ordfil,"%d",&antalord);
    for (b=0; b<antalord; b++) {
        fscanf(ordfil,"%s",testord);
        int poang = beraknapoang(testord,bokstav); //totalpo�ng ber�knas och sparas
        if (poang>totalpoang) { //j�mf�relsen sparar ej po�ng-dubletter
            totalpoang=poang;
            strcpy(maxord,testord);
        }
        fixaantal(bokstav); //�terst�ller tempantalsv�rden till ursprungsv�rden (antal)
    }
    printf("%s ger %d po�ng",maxord,totalpoang);
}
//FR�MJ�L �r fel!
