#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int antal = 0;

void print_num(int n){
    antal++;
    if(n<10) {
        printf("%d \n",n);
    } else {
        print_num(n/10);
        printf("%d ",n%10); //denna rad n�s endast i slutet
    }
}

int main(int argc, char* args[]) {
	print_num(1234);
	//printf("\nAntal varv: %d", antal);
}
