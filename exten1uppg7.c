//Exempeltenta 1, Uppgift 7 Dataförkortningar
#include <stdio.h>
int main(void){
    FILE *fil,*ut;
    int i,j,m,n,s,t=0;
    char kort[4],ord[30];
    ut=fopen("resultat.txt","wt");
    printf("Vilken förkortning (3 tkn): ");
    scanf("%s",kort);
    fil=fopen("dataord.txt","rt");
    fscanf(fil,"%d",&n);
    for(i=0;i<n;i++){
        fscanf(fil,"%s",ord);
        j=0; s=0;
        for(m=0;m<3;m++){
            while(ord[j]!=kort[m] && ord[j]!='\0')
            j++;
            if(ord[j]==’\0’)
            break;
            s++;
            j++;
        }
        if(s==3){
        t++;
        printf("%s\n",ord);
        fprintf(ut,"%s\n",ord);
        }
    }
    printf("Det fanns %d ord\n",t);
    fprintf(ut,"Det fanns %d ord\n",t);
    fclose(ut);
    fclose(fil);
}
