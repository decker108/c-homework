#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

int result = 0;

void jakt2(int n) {
    if (n == 42) {
        result = 1;
    } else if (n < 42) {
        return;
    } else {
        if (result == 1) {
            return;
        }
        if ((n%4==0 || n%3==0) && (((n%10)*((n/10)%10)) != 0)) {
            jakt2(n-((n%10)*((n/10)%10)));
        }
        if (n%2==0) {
            jakt2(n/2);
        }
        if (n%5==0) {
            jakt2(n-42);
        }
    }
}

int main(void) {
//    printf("%d \n",(52/10)%10);
//    printf("%d",(52%10)*((52/10)%10));
    int input = 0;
    while (1) {
        printf("Hur m\x86nga vargar finns nu? ");
        scanf("%d",&input);
        if (input > 42 && input < 10000) {
            break;
        }
    }
	jakt2(input);
	if (result == 1) {
	    printf("M\x86let kan n\x86s");
	} else {
	    printf("M\x86let kan INTE n\x86s");
	}
	return 0;
}
