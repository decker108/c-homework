//CL060111 Uppg 6 Personalstatistik
#include <stdio.h>
#include <stdlib.h>
int main(void) {
	//variabler
	int antal_anstallda,nuvarande_anstallda=0,test_year,test_month;
	int start_year,start_month,end_year,end_month;
	int testdatum,startdatum,slutdatum;

	//deklarera och �ppna fil
	FILE *fil;
    fil=fopen("personal.txt","rt");
    fscanf(fil,"%d", &antal_anstallda);

    //Ange �r och m�nad f�r s�kningen
    printf("Ange �rtal: ");
    scanf("%d",&test_year);
    printf("Ange m�nad: ");
    scanf("%d",&test_month);
    testdatum=test_year*100+test_month;

    //huvudloopen, kolla om alla anst�llda var anst�llda vid testdatumet.
    for (int a=1; a<=antal_anstallda; a++) {
        //l�s in startdatum och slutdatum.
    	fscanf(fil, "%d", &start_year);
    	fscanf(fil, "%d", &start_month);
    	fscanf(fil, "%d", &end_year);
    	fscanf(fil, "%d", &end_month);
    	startdatum=start_year*100+start_month; //tex 1955*100+04=195504
    	slutdatum=end_year*100+end_month;
    	if (startdatum<=testdatum && slutdatum>=testdatum) { //om personen b�rjade innan/p� testdatum och slutade efter/p� testdatum
            nuvarande_anstallda++;
    	}
    }

    //skriv ut resultatet och st�ng filen
    printf("Firman hade d� %d anst�llda.",nuvarande_anstallda);
    fclose(fil);
}
/*
Datans ordning i filen:
start_�r start_m�nad slut_�r slut_m�nad
*/
