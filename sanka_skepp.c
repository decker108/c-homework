//CL040819 Uppg5 S�nka skepp
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int skeppskoll(char spelplan[10][11]) {
    int rad,kolumn;
    for (rad=0; rad<10; rad++) {
    	for (kolumn=0; kolumn<10; kolumn++) {
    		if (spelplan[rad][kolumn]=='x') {
    		    return 0;
    		}
    	}
    }
    return 1;
}

int main(void) {
	//variabler
	int x=0,y=0,antal_skott=0,rad;
	char spelplan[10][11];
	FILE *fil;
	fil=fopen("skepp.txt","rt");

	//l�s in spelplanen
	for (rad=0; rad<10; rad++) {
		fscanf(fil,"%s",spelplan[rad]);
		printf("%s \n",spelplan[rad]); //debug-rad
	}

	while (1) {
        fscanf(fil,"%d %d",&y,&x); //l�s in koordinater (y=rad, x=kol)
        antal_skott++;
        if (spelplan[y-1][x-1]=='x') { //kontrollera om det �r en tr�ff, markera rutan
            spelplan[y-1][x-1]=0;
        }
        if (skeppskoll(spelplan)==1) { //kontrollera om det finns n�gra skepp kvar, om nej = break
            break;
        }
	}

	printf("Alla skepp nedskjutna efter %d skott",antal_skott);
}
