//CL060111 Uppgift 5 R�rfirman
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void) {
	int antal_anstallda,antal_jobb;
	int person_a,person_b;
	int par[100][100]={0};
    FILE *fil;

	fil=fopen("arbeten.txt","rt");
	//fil=fopen("060111-5.txt","rt");
	fscanf(fil,"%d",&antal_anstallda);
	fscanf(fil,"%d",&antal_jobb);

    //l�s in varje jobb i filen
	for (int a=0; a<antal_jobb; a++) {
		fscanf(fil,"%d %d",&person_a,&person_b); //l�s in tv� anst�lldas nummer
        //denna if/else-sats ser till att samma tv� nummer hamnar p� samma platser
        if (person_a>person_b) {
            par[person_a][person_b]++;
        } else {
            par[person_b][person_a]++;
        }
	}

    //hitta ett maxtal och spara det f�r utskrift
    int max_a,max_b,max=1;
	for (int a=1; a<antal_anstallda; a++) {
		for (int b=1; b<antal_anstallda; b++) {
			if (par[a][b]>max) {
                max=par[a][b];
                max_a=a;
                max_b=b;
			}
		}
	}

	//skriv ut resultatet
	printf("%d och %d har gjort %d jobb tillsammans \n",max_a,max_b,max);
}
/*
1. L�s in max antal anst�llda
2. L�s in max antal jobb
3. L�s in ett jobb, best�ende av person_a och person_b i r�tt ordning
*/
