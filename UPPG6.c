//Ola Rende DP09
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main(void) {
	//variabler
	int skyn[9][10];
	int block[9][9];
	int kontroll[9]={1,2,3,4,5,6,7,8,9}; //om alla fack �r 1 s� �r svaret r�tt
	int siffror[9]={0}; //0,1,2,3,4,5,6,7,8
	int korrektrad=0,korrektkolumn=0,korrekt_svar=0;
	FILE *fil;

	fil=fopen("himlen.txt","rt");
	if (fil==NULL) {
        printf("Fil ej funnen (FOF)");
	}
	for (int rad=0; rad<9; rad++){
		for (int kolumn=0; kolumn<9; kolumn++){
			fscanf(fil,"%d",&skyn[rad][kolumn]);
		}
	}
	fclose(fil);

//Testutskrift av filen
//    for (int rad=0; rad<9; rad++){
//		for (int kolumn=0; kolumn<9; kolumn++){
//            printf("%d ",skyn[rad][kolumn]);
//		}
//		printf("\n");
//    }

    //g� igenom filen i 9*9 steg, titta efter ett block d�r talen 1-9 finns med
    int varv=0;
    int korrekta_siffror;
    for (int rad=0; rad<=6; rad++) {
        for (int kolumn=0; kolumn<=6; kolumn++){
            korrekta_siffror=0;
            //printf("K�r loopen...\n");
            varv++;
        	//ge v�rden till blocket
        	block[0][0]=skyn[rad][kolumn]; //rad 1, l�ngst till v�nster
        	block[0][1]=skyn[rad][kolumn+1]; //rad 1, mitten
        	block[0][2]=skyn[rad][kolumn+2]; //rad 1, l�ngst till h�ger
        	block[1][3]=skyn[rad+1][kolumn];
        	block[1][4]=skyn[rad+1][kolumn+1];
        	block[1][5]=skyn[rad+1][kolumn+2];
        	block[2][6]=skyn[rad+2][kolumn];
        	block[2][7]=skyn[rad+2][kolumn+1];
        	block[2][8]=skyn[rad+2][kolumn+2];

            //printf("%d ",block[0][0]);
        	//printf("%d ",block[0][1]);
        	//printf("%d \n",block[0][2]);

        	//kontrollera om blocken inneh�ller en stege
        	int index=0;
        	while (index<9){ //g�r fr�n 0-8
                for (int block_rad=0; block_rad<3; block_rad++){ //0,1,2
                    for (int block_kolumn=0; block_kolumn<9; block_kolumn++){ //0,1,2,3,4,5,6,7,8
                        if (block[block_rad][block_kolumn]==index+1) { //index 1,2,3,4,5,6,7,8,9
                            siffror[index]=siffror[index]+1;
                        }
                    }
                }
                if (siffror[0]==1 && siffror[1]==1 && siffror[2]==1 && siffror[3]==1 && siffror[4]==1 && siffror[5]==1 && siffror[6]==1 && siffror[7]==1 && siffror[8]==1) {
                    korrekt_svar=1;
                }
                //printf("%d",index);
                index++;
        	}

        	//kontrollera om stegen inneh�ller alla 9 siffror
        	if (korrekt_svar==1) {
                korrektrad=rad;
                korrektkolumn=kolumn;
                printf("R�tt!");
        	} else {
                //printf("Fel\n");
        	}
        }
    }
    printf("Rad %d Kolumn %d\n",korrektrad,korrektkolumn);
    //printf("Antal varv: %d \nSiffror: %d",varv,korrekta_siffror);
}
/*
Gr�nser f�r blocket
 F�r rader: rad 6
 F�r kolumner: kolumn 6
*/
