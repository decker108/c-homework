#include <stdio.h>
#include <stdlib.h>

struct perstyp{
  char namn[10];
  struct perstyp *pek[3];
};

typedef struct perstyp person;

person *laesin(char *filnamn){
  FILE *infil;
  int antal,a,i,j;
  person *adress[100],*start;
  char namn[10];

  infil=fopen(filnamn,"rt");
  fscanf(infil,"%d",&antal);
  for(i=0;i<antal;i++)
    adress[i]=(person *)malloc(sizeof(person));
  for(i=0;i<antal;i++){
    if (i==0) start=adress[i];
    fscanf(infil,"%s",adress[i]->namn);
    for(j=0;j<3;j++){
      fscanf(infil,"%d",&a);
      if (a==-1)
        adress[i]->pek[j]=NULL;
      else
        adress[i]->pek[j]=adress[a];
    }
  }
  fclose(infil);
  return start;
}

void tabort(person *start,person *slap,int nr){
  person *pek[3],*pekare;
  int i;
  pekare=start;
  for(i=0;i<3;i++)
    if (pekare->pek[i]!=NULL)
      tabort(pekare->pek[i],pekare,i);
  slap->pek[nr]=NULL;
  free(pekare);
}

