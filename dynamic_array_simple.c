#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
int main() {
    int* array;
    int n, i;
    printf("Enter the number of elements: ");
    scanf("%d", &n);
    array = (int*) malloc(n*sizeof(int));
    for (i=0; i<n; i++) {
        printf("Enter number %d: ", i);
        scanf("%d", &array[i]);
    }
    printf("\nThe Dynamic Array is: \n");
    for (i=0; i<n; i++) {
        printf("The value of %d is %d\n", i, array[i]);
    }
    printf("Size= %d\n", i);
    return 0;
}