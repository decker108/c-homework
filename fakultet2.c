#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int fact(int n) { //fakultet med rekursion
    if (n==0) {
        return 1;
    } else {
        return n*fact(n-1);
    }
}

int fact1(int n) {
    int i, p;
    p = 1;
    for (i=1; i<=n; i++) {
    	p = p * i;
    }
    return p;
}

int main(int argc, char* args[]) {
	printf("%d \n",fact(5));
	printf("%d \n",fact1(5));
}
