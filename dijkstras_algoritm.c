#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

void dijkstra(char graph[][3],int ne,int nv,char start,char stop,int path[]);

int main(void){
    char graph[][3]={{'A','B',2},{'A','F',1},{'B','D',2},{'D','F',1},
                     {'B','C',2},{'B','E',4},{'C','E',3},{'C','H',1},
                     {'D','E',4},{'E','G',7},{'G','H',6},{'F','G',5}};
    int path[256];
    dijkstra(graph,12,8,'A','H',path);
    printf("%d\n",path['H']);
    return 0;
}

void dijkstra(char graph[][3],int ne,int nv,char start,char stop,int path[]){
    int t[256],i,j,v,x,min;
    for(i='A';i<='A'+nv;i++){
        path[i]=INT_MAX;
        t[i]=1;
    }
    path[start]=0;
    
    while(t[stop]){
        min=INT_MAX;
        for(i='A';i<='A'+nv;i++) {
            if(t[i] && path[i]<min){
                v=i;
                min=path[i];
            }
        }
        t[v]=0;
        
        for(j=0;j<ne;j++) {
            x=-1;
            if(graph[j][0]==v) { 
                x=graph[j][1];
            }
            if(graph[j][1]==v) { 
                x=graph[j][0];
            }
            if(x>=0 && path[x]>path[v]+graph[j][2]) {
                path[x]=path[v]+graph[j][2];
            }
        }
    }
}
