//Exempeltenta 2, Uppg 2 Tal i rad
#include <stdio.h>
#include <stdlib.h>
int main(void){
    int antal,k,p,x,klar,n;
    int lista[9]={8,4,2,9,3,7,5,6,1}; //talen som ska anv�ndas
    n=0; //antal drag
    do {
        for (k=0; k<9; k++) { //skriv ut hela listan
            printf("%3d",lista[k]);
        }
        printf(" ? ");
        scanf("%d",&antal);
            for (k=0; k<antal/2; k++) { //Endast h�lften av siffror beh�ver g�s igenom
                x=lista[k];
                lista[k]=lista[antal-k-1]; //byt plats till vad�? (0-1-1=-2)
                lista[antal-k-1]=x;
            }
        n++; //�ka antalet f�rs�k
        klar=1; //talserien godk�nd by default
        for (k=0; k<9; k++) {
            if (lista[k]!=k+1) { //om siffran k inte �r lika med siffran efter...
                klar=0; //...talserien inte r�tt.
            }
        }
    } while (!klar); //k�r s� l�nge klar inte �r 1 (s� l�nge klar �r 0)
    printf("  1  2  3  4  5  6  7  8  9\n");
    printf("Grattis, du klarade det p� %d v�ndningar\n",n);
}
/*
Programmet ger en lista med talen 8 4 2 9 3 7 5 6 1.
Anv�ndaren ska sortera talen i ordningen 1-9.
Varje drag �r ett antal siffror som sorteras i omv�nd ordning.
H�lften av siffror byter plats, ev mittsiffra st�r kvar.
*/
