//CL050602 Uppg7 Orientering
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int orientera(int k1_x,int k1_y,int k2_x,int k2_y,int k3_x,int k3_y,int k4_x,int k4_y,FILE *fil){
	int index=0;
	char input;
	int pos_x=1;
	int pos_y=1;
	int k1flagga=0,k2flagga=0,k3flagga=0,k4flagga=0;
	int antal_steg=0;
	char superstrang[5001];

	fscanf(fil,"%5000s",superstrang);

	while (superstrang[index]!=NULL) {
	    //f�rst kollas vilket tecken som l�sts in
	    input=superstrang[index];
        //inputtecknet j�mf�rs med de fyra m�jliga valen
	    if (input=='N') {
	    	pos_y--;
	    } else if (input=='S') {
	        pos_y++;
	    } else if (input=='W') {
	        pos_x--;
	    } else if (input=='E') {
            pos_x++;
	    }
        antal_steg++;
        index++;
        if (pos_x==k1_x && pos_y==k1_y) {
            k1flagga=1;
        }
        if (k1flagga==1 && pos_x==k2_x && pos_y==k2_y) {
            k2flagga=1;
        }
        if (k2flagga==1 && pos_x==k3_x && pos_y==k3_y) {
            k3flagga=1;
        }
        if (k3flagga==1 && pos_x==k4_x && pos_y==k4_y) {
            k4flagga=1;
        }
        if (k4flagga==1 && pos_x==10 && pos_y==10) {
            break;
        }
	}

    return antal_steg;
}

int main(void) {
	//variabler
	int antal_steg;
	int k1_x,k1_y,k2_x,k2_y,k3_x,k3_y,k4_x,k4_y;
	FILE *fil;
	fil=fopen("orientering.txt","rt");

	//l�s in positionerna f�r kontrollerna
	fscanf(fil,"%d",&k1_x);
	fscanf(fil,"%d",&k1_y);
	fscanf(fil,"%d",&k2_x);
	fscanf(fil,"%d",&k2_y);
	fscanf(fil,"%d",&k3_x);
	fscanf(fil,"%d",&k3_y);
	fscanf(fil,"%d",&k4_x);
	fscanf(fil,"%d",&k4_y);

    //ett funktionsanrop med resultatsutskrift per spelare
    antal_steg=orientera(k1_x,k1_y,k2_x,k2_y,k3_x,k3_y,k4_x,k4_y,fil);
    printf("L�pare 1 g�r i m�l efter %d steg \n",antal_steg);
    antal_steg=orientera(k1_x,k1_y,k2_x,k2_y,k3_x,k3_y,k4_x,k4_y,fil);
    printf("L�pare 2 g�r i m�l efter %d steg \n",antal_steg);
    antal_steg=orientera(k1_x,k1_y,k2_x,k2_y,k3_x,k3_y,k4_x,k4_y,fil);
    printf("L�pare 3 g�r i m�l efter %d steg \n",antal_steg);
    antal_steg=orientera(k1_x,k1_y,k2_x,k2_y,k3_x,k3_y,k4_x,k4_y,fil);
    printf("L�pare 4 g�r i m�l efter %d steg \n",antal_steg);

    fclose(fil);
}
