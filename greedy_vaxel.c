#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

void funk(int valorer[8], int antal, int belopp) {
    int org = belopp;
    int n = 0;
    int i;
    for (i=antal; i>=0; i--) {
    	while (belopp >= valorer[i]) {
    	    belopp = belopp - valorer[i];
    	    n = n + 1;
    	}
    }
    //if (n >= 13)
    printf("Beloppet %d kr\x84ver %d st mynt\n",org,n);
}

int main(void) {
    int b;
//    printf("Belopp: ");
//    scanf("%d",&belopp);
    int valorer[8] = {1,5,10,20,50,100,500,1000};
    for (b = 1; b<1000; b++) {
    	funk(valorer,8,b);
    }
	return 0;
}
