//CL050825 Uppgift 7 Billboard
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct hit{
    int year;
    int place;
    char artist[100];
    char title[100];
};

int main(void) {
	//variabler
	struct hit listpost;
	char input_namn[100]; //namnet som anv�ndaren skriver in.
	int totalpoang=0; //ber�knas med placering*po�ngskala.
	int poangskala[101]={0}; //ska g� mellan 1-100, med sjunkande po�ng 100-1.
	FILE *musikfil;

	//l�s in namnet
	printf("Artist? ");
	gets(input_namn); //gets kr�vs f�r att l�sa in namn med mellanslag

	//best�m v�rdena i po�ng-arrayen. Po�ngen �r 100p f�r plats 1, 99p f�r 2, etc...
	int poang=100;
	for (int a=1; a<=100; a++) {
	    poangskala[a]=poang;
	    poang--;
	}

	//g� igenom filen och se om artistens namn finns med
	musikfil=fopen("hits.dat","rb");
	while (1) {
	//for (int a=0; a<10; a++) { //anv�nd f�r att testa delar av listan
	    //fread-satsen l�ser in data fr�n musikfil till struct'en hit listpost. F�r varje post l�ses 2 int-variabler och 2 100-plats str�ngar in.
	    int n = fread(&listpost,sizeof(struct hit),1,musikfil);
	    if (n!=1) { //s�l�nge en hel post kan l�sas �r n=1, efter det stoppas loopen.
            break;
	    }
	    //test-utskrift placeras h�r!
	    int jmf = strcmp(input_namn,listpost.artist);
	    if (jmf==0) {
            totalpoang+=poangskala[listpost.place];
	    }
	}
	rewind(musikfil);

	//skrivut resultat och st�ng filen
	printf("Totalpo�ng: %d \n",totalpoang);
	fclose(musikfil);
}
/*
Antal poster i filen: 4410

1. Fr�ga efter artistens namn som s�kstr�ng.
2. L�s igenom listan och titta efter namnet. F�r varje tr�ff, spara en post i struct.
3. G� igenom struct och ber�kna totalpo�ng.

fread(&listpost,sizeof(struct hit),1,musikfil);
1 = i vilken variabel ska datan lagras?
2 = hur stor plats tar datan (i byte)?
3 = antalet poster datan tar upp?
4 = fil-variabeln?

printf("Artistnamn: %s \nTitel: %s \n�r: %d \nNr: %d\n",listpost.artist,listpost.title,listpost.year,listpost.place);
*/
