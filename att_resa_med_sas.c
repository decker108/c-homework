//CL050307 Uppg 3 Att resa med SAS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct prispost {
    char namn[20];
    float enkel;
    float turoretur;
    float overvikt;
};

int main(void) {
	//variabler
	struct prispost post;
	char destination[20];
	int val;
	float input_overvikt;
	float totalpris;
	FILE *fil;

	//samla indata
	printf("Destination? ");
	scanf("%s",destination);
	printf("Enkel resa (1) eller Tur och Retur (2)? ");
	scanf("%d",&val);
	printf("�vervikt (kg): ");
	scanf("%f",&input_overvikt);

    //l�s igenom bin�rfilen, leta efter resultatet
	fil=fopen("flygpriser.dat","rb");
	while (!feof(fil)) {
	    fread(&post,sizeof(struct prispost),1,fil);
	    int jmf=strcmp(destination,post.namn);
	    if (jmf==0) {
	        if (val==1) { //f�r enkelbiljetter
	            totalpris=(post.overvikt)*input_overvikt+(post.enkel);
	        } else { //f�r turoretur-biljetter
	            totalpris=(post.overvikt)*input_overvikt+(post.turoretur);
	        }
	        break;
	    }
	}

	//skriv resultatet
    printf("Biljetten till %s kostar %.2f kr",post.namn,totalpris);
    fclose(fil);
}
