#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

int antal = 0, n = 0;

void planka(int len) {
    if (len == n) {
        antal++;
    } else {
        if (len+3 <= n) {
            planka(len+3);
        }
        if (len+2 <= n) {
            planka(len+2);
        }
        if (len+1 <= n) {
            planka(len+1);
        }
    }
}

int main(void) {
    while (1) {
        printf("Plankans l\x84ngd: ");
        scanf("%d",&n);
        if (n >= 1 && n <= 24) {
            break;
        }
    }
	planka(0);
	printf("Det finns %d olika s\x84tt",antal);
	return 0;
}