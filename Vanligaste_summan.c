//CL080119 Uppgift 7 Vanligaste summan
#include <stdio.h>
#include <stdlib.h>

int main(void) {
    int talen[15]={3,5,7,14,18,23,32,44,51,53,62,71,83,84,96};
    int tal_antal = sizeof talen/sizeof talen[0];
    int talsumma=0;

    //best�m jmfsumma-arrayens maxv�rde.
    int max=0;
    for (int e=0; e<=tal_antal; e++) {
        if (talen[e]>max) {
            max=talen[e];
        }
    }

    //jmfsumma-pekarens array-index f�r v�rdet av det h�gsta talet multiplicerat med 3.
    int *jmfsumma=(int *)malloc(3*max*sizeof(int));

    //nollst�ll arrayen
    for (int f=0; f<=(3*max); f++) {
        //printf("%d ",jmfsumma[f]);
        jmfsumma[f]=0;
    }
    printf("\n");

    //g� igenom de alla olika unika kombinationer, spara summornas frekvens.
    for (int a=0; a<tal_antal; a++) { //g�r fr�n noll till femton
        for (int b=a; b<tal_antal; b++) { //g�r fr�n a till femton, dock ej samma som a
            for (int c=b; c<tal_antal; c++) { //g�r fr�n b till femton, ej samma som b
                if (talen[a]!=talen[b] && talen[a]!=talen[c] && talen[b]!=talen[c]) {
                    talsumma=talen[a]+talen[b]+talen[c]; //ber�kna talens summa
                    jmfsumma[talsumma]++; //�kar frekvensen p� talen.
                    //printf("%d %d %d \n",a,b,c);
                }
            }
        }
    }

    //hitta f�rsta b�sta talsumman med h�gst frekvens.
    int max_summa_frekv=0, max_summa=0;
    for (int d=0; d<=(3*max); d++) {
        if (jmfsumma[d]>max_summa_frekv) {
            max_summa_frekv=jmfsumma[d];
            max_summa=d;
        }
    } //skriv ut svaret.
    printf("Summan %d erh�lls %d g�nger",max_summa,max_summa_frekv);
}
/*
anv�nd 3 loopar f�r att addera tre olika tal med alla m�jliga kombinationer.
spara antalet g�nger man f�r samma summa.

kolla att inga index upprepas i olika ordning. (klart)
f�r varje summa leta upp summan i frekv_tabell och �ka antal eller l�gg till ny summa i
frekv_tabell och �ka frekv_antal.
*/
