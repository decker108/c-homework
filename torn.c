#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

int leftTarget,rightTarget,poss=0;

void torn(int a[], int n, int v[], int p) {
    int i;
    if (n==p) {
        int leftCount=0,rightCount=0,max=-1;
        //testa torn-arrayen fr�n h�ger och v�nster
        for (i=0; i<n; i++) {
        	if (i == 0 || a[i] > max) {
                leftCount++;
        	}
        	if (a[i]>max) {
                max = a[i];
        	}
        }
        max=-1;
        for (i=n-1; i>=0; i--) {
        	if (i == n-1 || a[i] > max) {
                rightCount++;
        	}
        	if (a[i]>max) {
                max = a[i];
        	}
        }
        if (rightCount == rightTarget && leftCount == leftTarget) {
            poss++;
        }
    } else {
        for (i=0; i<n; i++) {
        	if (!v[i]) {
        	    v[i] = 1;
        	    a[p] = i;
        	    torn(a,n,v,p+1);
        	    v[i] = 0;
        	}
        }
    }
}

int main(void) {
    int antal;
    printf("Antal torn: ");
    scanf("%d",&antal);
    printf("Ses fr\x86n v\x84nster: ");
    scanf("%d",&leftTarget);
    printf("Ses fr\x86n h\x94ger: ");
    scanf("%d",&rightTarget);
    int a[11],v[11]={0};
	torn(a,antal,v,0);
	printf("Det finns %d m\x94jligheter att st\x84lla upp tornen \n",poss);
	return 0;
}
