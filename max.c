#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

int max(int a[], int low, int high) {
    int m1,m2;
    if (low == high) {
        return a[low];
    } else {
        m1 = max(a,low,(low+high)/2);
        m2 = max(a,(low+high)/2+1,high);
        if (m1>m2) {
            return m1;
        } else {
            return m2;
        }
    }
}

int main(void) {
	int a[10] = {30,71,43,72,99,55,32,25,67,90};
	printf("max: %d",max(a,0,10));
	return 0;
}
