//CL040413 Fotoalbumet
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int max(int a, int b) {
    return a < b ? b : a; //om a �r mindre �n b returneras b, annars returneras a.
}

int main(void) {
	//variabler
	FILE *fil;
	fil=fopen("foton.txt","rt");
	int antal;
	fscanf(fil,"%d",&antal);

    int x;
    int x_sida[6];
    int y_sida[6];
	for (x=0; x<antal; x=x+1) {
		fscanf(fil,"%d %d",&x_sida[x],&y_sida[x]);
	}
    fclose(fil);

	//ber�kna den minsta arean med hj�lp av tv� sidor som ALLA andra sidor passar i.
    int a;
    int max_x=0,max_y=0;
    for (a=0; a<antal; a++) {
        int max_x1=max(max_x,x_sida[a]); //hitta tempor�r st�rsta x-sida
        int max_y1=max(max_y,y_sida[a]); //hitta tempor�r st�rsta y-sida

        int max_x2=max(max_x,y_sida[a]); //samma som ovan med omv�nda x/y-sidor
        int max_y2=max(max_y,x_sida[a]); //samma som ovan med omv�nda x/y-sidor

        if (max_x1*max_y1<max_x2*max_y2) { //vilket alternativ ger minst area
            max_x=max_x1;
            max_y=max_y1;
        } else {
            max_x=max_x2;
            max_y=max_y2;
        }
    }

    printf("Sidans dimensioner %d x %d ger arean %d",max_x,max_y,max_x*max_y);
}
