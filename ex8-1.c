/*
 ============================================================================
 Name        : ex8-1.c
 Author      : Ola Rende, DP09
 Version     :
 Copyright   : 
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

// 0 = mur, 1 = v�g
int maze[13][13] = {{0,0,0,0,0,0,0,0,0,0,0,0,0},
					{0,1,1,1,1,1,1,1,0,1,1,1,0},
					{0,1,0,0,0,0,0,1,0,0,0,1,0},
					{0,1,1,1,1,1,0,1,1,1,1,1,0},
					{0,1,0,0,0,1,0,1,0,0,0,0,0},
					{0,1,1,1,1,1,0,1,1,1,1,1,0},
					{0,0,0,1,0,0,0,0,0,1,0,1,0},
					{0,1,0,1,1,1,1,1,0,1,0,1,0},
					{0,1,0,0,0,1,0,0,0,1,0,0,0},
					{0,1,1,1,0,1,0,1,1,1,1,1,0},
					{0,0,0,1,0,1,0,1,0,0,0,0,0},
					{0,1,1,1,1,1,0,1,1,1,1,1,0},
					{0,0,0,0,0,0,0,0,0,0,0,0,0}};

int destCol = 11, destRow = 11;

void solveMaze(int col, int row, int steps);

int main(void) {
	solveMaze(7,1,0);
	return EXIT_SUCCESS;
}

void solveMaze(int row, int col, int steps) {
	maze[row][col] = 0;
	if (row == destRow && col == destCol) {
		printf("Reached goal at (%d,%d) after %d steps \n",col,row,steps);
		exit(0);
	} else {
		if (maze[row-1][col] == 1) { //possible to go north?
			solveMaze(row-1,col,steps+1);
		}
		if (maze[row+1][col] == 1) { //possible to go south?
			solveMaze(row+1,col,steps+1);
		}
		if (maze[row][col-1] == 1) { //possible to go west?
			solveMaze(row,col-1,steps+1);
		}
		if (maze[row][col+1] == 1) { //possible to go east?
			solveMaze(row,col+1,steps+1);
		}
	}
	maze[row][col] = 1;
}
