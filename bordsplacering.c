#include <stdio.h>
#include <assert.h>

#define MAXNUM 10

//char x = 'A';
int max = 0;
int array[MAXNUM][MAXNUM];

// a[] = talarrayen, n = maxtalet-1 i permutationen.
// v[] = array som h�ller koll p� anv�nda siffror, p = antal tal som anv�nts i a. 
void solve(int a[],int n,int v[],int p){
    int i,tal;
    if(n == p){ //om ett l�v har n�tts i rekursionstr�det, skriv ut talen
        int sum = 0;
        for (i=0; i<n; i++) {
        	sum += array[i][a[i]];
        }
        if (sum>max) {
            max = sum;
        }
    } else {
        for(tal=0;tal<n;tal++) {
            if (!v[tal]) {  //om talet v[i] ej �r anv�nt...
                v[tal] = 1; //markera tal som anv�nt
                a[p] = tal; //l�gg till talet i permutationsarrayen
                solve(a,n,v,p+1); //g� till n�sta index i arrayen
                v[tal] = 0; //avmarkera tal som oanv�nt
            }
        }
    }
}

int main(void) {
    int n,i,j,x;
    int a[MAXNUM],v[MAXNUM]={0};
    FILE *fil;
    fil = fopen("party.dat","rt");
    assert(fil != NULL);
    fscanf(fil,"%d",&n);
    for (j=0; j<n; j++) {
        for (i=0; i<n; i++) {
            fscanf(fil,"%d",&x);
            array[j][i] = x;
        }
    }
    solve(a,n,v,0);
    printf("H\x94gsta m\x94jliga trivselpo\x84ng \x84r %d",max);
    return 0;
}
