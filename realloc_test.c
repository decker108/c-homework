#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

struct strTyp{
    char str[10];
};

void funk() {
    FILE *fil;
    fil = fopen("strtest.dat","rt");
    if (fil == NULL) {
        exit(-1);
    }

    char input[10];
    int count=0;
    struct strTyp *strings = NULL;
    struct strTyp *more_strings;

    while (!feof(fil)) {
        fscanf(fil,"%s",input);
        count++;

        more_strings = (struct strTyp*) realloc(strings, count*sizeof(struct strTyp));

        if (more_strings != NULL) {
            strings = more_strings;
            //printf("%p",string);
            //strings[count-1] = input;
            strcpy(strings[count-1].str,input);
        } else {
            free(strings);
            puts("Error (re)allocating memory");
            exit(1);
        }
    }

    printf("Strings entered: ");
    int n;
    for (n=0; n<count; n++) {
        printf("%s ",strings[n].str);
    }
    printf("\nNumber of strings: %d",count);
    free(strings);

    fclose(fil);
}

void funk2() {
    FILE *fil;
    fil = fopen("strtest2.dat","rt");
    if (fil == NULL) {
        exit(-1);
    }

    int input,n;
    int count = 0;
    int *numbers = NULL;
    int *more_numbers;

    while (!feof(fil)) {
        fscanf(fil,"%d",&input);
        count++;

        more_numbers = (int*) realloc(numbers, count * sizeof(int));

        if (more_numbers != NULL) {
            numbers = more_numbers;
            numbers[count-1] = input;
        } else {
            free(numbers);
            puts("Error (re)allocating memory");
            exit(1);
        }
    }

    printf("Numbers entered: ");
    for (n=0; n<count; n++) {
        printf("%d ",numbers[n]);
    }
    free(numbers);

    fclose(fil);
}

void funk3() {
    FILE *fil;
    fil = fopen("strtest.dat","rt");
    assert(fil != NULL);
    
    char input[10];   //char f�r inl�sning
    char **strMatrix; //str�ngmatrisens dubbelpekare
    int count = 0;    //antal inl�sta str�ngar
    while (!feof(fil)) {
        fscanf(fil,"%s",input);
        count++;
        
        //omallokera fler pekare (ungef�r: �ka antal rader i matrisen)
        strMatrix = (char*) realloc(strMatrix, count*sizeof(char*));
        //printf("%p\n",strMatrix); //skriv ut pekar-adressen
        
        //allokera char-platser i matrisens senaste rad
        strMatrix[count-1] = malloc((strlen(input)+1)*sizeof(char));
        
        //kopiera in inl�sta str�ngen p� senaste raden
        strcpy(strMatrix[count-1],input);
    }
    
    int i;
    for (i=0; i<count; i++) {
    	printf("%s ",strMatrix[i]);
    }
    
    fclose(fil);
}

void funk4() {
    FILE *fil;
    fil = fopen("strtest.dat","rt");
    assert(fil != NULL);
    
    char input[10];   //char f�r inl�sning
    char **strMatrix; //str�ngmatrisens dubbelpekare
    int count = 0;    //antal inl�sta str�ngar
    //size_t memSpace = 0;
    while (!feof(fil)) {
        fscanf(fil,"%s",input);
        count++;
        
        //allokera plats, f�rdubbla storleken per allokering
    }
    
    int i;
    for (i=0; i<count; i++) {
    	printf("%s ",strMatrix[i]);
    }
    
    fclose(fil);
}

int main(int argc, char* args[]) {
    //funk(); //dynamisk allokering av struct-array
    //funk2(); //dynamisk allokering av int-array
    //funk3();
    funk4();
    
    //printf("res=%d",strcmp("HEJ",NULL)); //error

    return 0;
}
