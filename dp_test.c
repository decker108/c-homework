#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

int tab[10][10];
int mem[10][10];
int n,max_s=0,x=0;

void berakna(int rad,int kol,int s) {
    if(rad==n-1){
        s=s+tab[rad][kol];
        if(s>max_s) {
            max_s=s;
        }
    } else {
        berakna(rad+1,kol,  s+tab[rad][kol]);
        berakna(rad+1,kol+1,s+tab[rad][kol]);
    }
}

int max(int a, int b) {
    if (a>b) {
        return a;
    } else {
        return b;
    }
}

int berakna2(int rad, int kol, int s) {
    if(rad==n-1){
        return s+tab[rad][kol];
    } else {
        return max( berakna2(rad+1,kol,  s+tab[rad][kol]),
                    berakna2(rad+1,kol+1,s+tab[rad][kol]));
    }
}

int berakna5(int rad, int kol) {
    if (mem[rad][kol] != -1) {
        return mem[rad][kol];
    }
    if(rad==n-1) {
        return tab[rad][kol];
    } else {
        int a,b;
        a = berakna5(rad+1,kol);
        b = berakna5(rad+1,kol+1);
        mem[rad][kol] = tab[rad][kol]+max(a,b);
        return mem[rad][kol];
    }
}

int berakna4(int rad, int kol, int s) {
    if(rad==n-1){
        return s+tab[rad][kol];
    } else {
        int a,b;
        a = berakna4(rad+1,kol,  s+tab[rad][kol]);
        b = berakna4(rad+1,kol+1,s+tab[rad][kol]);
        mem[rad][kol] = max(a,b);
    }
}

int berakna3(int rad, int kol, int s) {
    mem[rad][kol] = s+tab[rad][kol];
    if(rad==n-1) {
        return s+tab[rad][kol];
    } else {
        int a,b;
        if (mem[rad+1][kol] == -1) {
            a = berakna3(rad+1,kol,  s+tab[rad][kol]);
        } else {
            a = mem[rad+1][kol];
        }
        if (mem[rad+1][kol+1] == -1) {
            b = berakna3(rad+1,kol+1,s+tab[rad][kol]);
        } else {
            b = mem[rad+1][kol+1];
        }
        return max(a,b);
    }
}

int main(void){
    FILE *infil;
    int i,j,k,l;
    infil=fopen("input.dat","rt");
    fscanf(infil,"%d",&n);
    for(i=0;i<n;i++) {
        for(j=0;j<=i;j++) {
            fscanf(infil,"%d",&tab[i][j]);
        }
    }
    for (k=0; k<n; k++) {
    	for (l=0; l<n; l++) {
    		mem[k][l] = -1;
    	}
    }
    fclose(infil);
    
//    berakna(0,0,0);
//    printf("Max = %d",max_s);
//    printf("Maximal summa: %d\n",berakna2(0,0,0));
//    printf("Maximal summa: %d\n",berakna3(0,0,0));
//    printf("Antal anrop = %d \n",x);
    printf("Maximal summa: %d\n",berakna5(0,0));
    return 0;
}
