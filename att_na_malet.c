//CL050307 Uppg5 - Att n� m�let
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
int main(void) {
	//variabler
	int antal_lyckade=0;
	int upp,ned,hoger,vanster;
	srand(time(0));

	//l�s in sannolikheter f�r upp,ned,v�nster
	printf("Sannolikhet UPP: ");
	scanf("%d",&upp);
	printf("Sannolikhet NED: ");
	scanf("%d",&ned);
	printf("Sannolikhet V�NSTER: ");
	scanf("%d",&vanster);

	//ber�kna % f�r h�ger
	hoger=100-upp-ned-vanster;

	//g�r 100000 vandringar
        //om antal_steg==200 eller position==(20,20) �r vandringen slut och ev lyckad
    int i;
    for (i=0; i<100000; i++) {
        int limit=0,pos_y=0,pos_x=0,antal_steg=0;
    	while (1) {
    	    int flagga=0;
            int slumptal=rand()%100;
            limit=upp;
            if (slumptal<limit) {
                pos_y++;
                antal_steg++;
                flagga=1;
            } else {
                limit+=ned;
            }
            if (flagga==0 && slumptal<limit) {
                pos_y--;
                antal_steg++;
                flagga=1;
            } else {
                limit+=vanster;
            }
            if (flagga==0 && slumptal<limit) {
                pos_x--;
                antal_steg++;
                flagga=1;
            } else {
                limit+=hoger;
            }
            if (flagga==0 && slumptal<limit) {
                pos_x++;
                antal_steg++;
            }
            if (pos_x==20 && pos_y==20) {
                antal_lyckade++;
                break;
            } else if (antal_steg==200) {
                break;
            }
        }
    }

    //ber�kna (antal lyckade vandringar)/100000
        //skriv resultat
    printf("%.2f%% av vandringarna var LYCKADE",(antal_lyckade/100000.0)*100.0);
}
