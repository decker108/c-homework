#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

int min_antal = INT_MAX;

void lek(int blue, int white, int red, int antal) {
    if (antal >= 15) {
        return;
    } else if (blue == white && white == red && blue == red) {
        if (antal < min_antal) {
            min_antal = antal;
        }
    } else {
        lek(blue-1,white+3,red+1,antal+1);
        lek(blue+3,white-1,red+4,antal+1);
        lek(blue+2,white+2,red-1,antal+1);
    }
}

int main(void) {
    int blue = 0,white = 0,red = 0;
	printf("Hur m\x86nga bl�a? ");
	scanf("%d",&blue);
	printf("Hur m\x86nga vita? ");
	scanf("%d",&white);
	printf("Hur m\x86nga r�da? ");
	scanf("%d",&red);
	lek(blue,white,red,0);
	printf("Det beh\x94vs minst %d v\x84xlingar f\x94r att n\x86 m\x86let",min_antal);
	return 0;
}
/*
bl� = 3*vit + 1*r�d
vit = 3*bl� + 4*r�d
r�d = 2*vit + 2*bl�
*/