//CL050602 Uppgift 8 Ordspr�k
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct ordsprak{
    char text[200];
};

int main(void) {
	//variabler
	struct ordsprak mening;
	char input_string[20];
	FILE *fil;

    //fr�ga efter s�kstr�ng och best�m dess l�ngd
	printf("Vilket ord? ");
	scanf("%s",input_string);

	fil=fopen("ordsprak.dat","rb");
	while (1) {
//    for (int a=0; a<=10; a++) {
        int n = fread(&mening,sizeof(struct ordsprak),1,fil);
        if (n!=1) {
            break;
        }
        int i = 0;
        while (mening.text[i]!='\0') { //loopen g�r tills radens slut
            //hoppa �ver mellanslag (tex i b�rjan)
            while (mening.text[i]==' ') {
                i++;
            }
            //sparar startpositionen f�r ordet
            int ordstart = i;
            //hoppa �ver tills mellanslag eller radslut
            while (mening.text[i]!='\0' && mening.text[i]!=' ') {
                i++;
            }
            //bryt vid slut p� rad
            if (mening.text[i]=='\0') {
                break;
            }
            //printf("%s \n%s \n%d \n",&mening.text[ordstart],&mening.text[i],i-ordstart);
            //j�mf�r s�kord med b�rjan (ordstart) till slutet av ordet.
            if (strncmp(input_string,&mening.text[ordstart],i-ordstart)==0) { //skillnad mellan nuvarande position och startpos.
                //hittas ett matchande par, skriv ut och bryt.
                printf("%s \n",mening.text);
                break;
            }
        }
	}
	fclose(fil);
}
/*
metastr�ng-funk: strstr(str1,str2);

1. L�s in s�k-str�ng
2. l�s igenom bin�rfilen och j�mf�r med s�kstr�ng.
   2a. Kolla str�ngl�ngd f�r s�kstr�ng, j�mf�r med det som hittas i s�kningen.
   2b. Om l�ngderna st�mmer, skriv ut.
*/
