#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

int max = 0;
int anrop = 0;

void solve (int rad,int kol,int tab[][9],int f[][9],int n,int sum) {
    anrop++;
    sum = sum + tab[rad][kol];
    if (n == 10) {
        if(sum > max) {
            max = sum;
        }
    } else {
        f[rad][kol] = 1;
        if (rad>0 && f[rad-1][kol]==0) {
            solve(rad-1,kol,tab,f,n+1,sum);
        }
        if (rad<8 && f[rad+1][kol]==0) {
            solve(rad+1,kol,tab,f,n+1,sum);
        }
        if (kol>0 && f[rad][kol-1]==0) {
            solve(rad,kol-1,tab,f,n+1,sum);
        }
        if (kol<8 && f[rad][kol+1]==0) {
            solve(rad,kol+1,tab,f,n+1,sum);
        }
        f[rad][kol] = 0;
    }
}

int main(void){
    int tab[9][9] = {   {3, 1, 4, 1, 5, 9, 2, 6, 5},
                        {3, 5, 8, 9, 7, 9, 3, 2, 3},
                        {8, 4, 6, 2, 6, 4, 3, 3, 8},
                        {3, 2, 7, 9, 5, 0, 2, 8, 8},
                        {4, 1, 9, 7, 1, 6, 9, 3, 9},
                        {9, 3, 7, 5, 1, 0, 5, 8, 2},
                        {0, 9, 7, 4, 9, 4, 4, 5, 9},
                        {2, 3, 0, 7, 8, 1, 6, 4, 0},
                        {6, 2, 8, 6, 2, 0, 8, 9, 9}};
    int f[9][9]; //f-matrisen anv�nds f�r att markera anv�nda rutor
    int i,j;
    for(i=0;i<9;i++) {
        for(j=0;j<9;j++){
            f[i][j]=0; //nollst�ll f matrisen innan anv�ndning
        }
    }
    for(i=0;i<9;i++) {
        for(j=0;j<9;j++) {
            solve(i,j,tab,f,1,0); //prova alla kombinationer p� en ruta i taget
        }
    }
    printf("Maximala summan: %d\nAntal anrop: %d",max,anrop);
    
    return 0;
}
