//CL050307 Uppg 6 Vilka drag �r m�jliga?
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main(void) {
	int spelplan[5][5];
	FILE *fil;
	fil=fopen("stallning.txt","rt");

	int rad,kolumn;
	for (rad=0; rad<4; rad++) {
		for (kolumn=0; kolumn<4; kolumn++) {
			fscanf(fil,"%d",&spelplan[rad][kolumn]);
		}
	}
	fclose(fil);

	int antal_A=0,antal_B=0;
	for (rad=0; rad<5; rad++) {
		for (kolumn=0; kolumn<5; kolumn++) {
			if (spelplan[rad][kolumn]==1) { //Om rutan inneh�ller en vit pj�s...
			    if (spelplan[rad-1][kolumn]==0) {  //Kolla om drag av kat A �r m�jligt
                    antal_A++;
			    }
			    if (spelplan[rad-1][kolumn-1]==2) {
			        antal_B++;
			    }
			    if (spelplan[rad-1][kolumn+1]==2) {
			        antal_B++;
			    }
			}
		}
	}

	printf("Det finns %d drag av kategori A\n",antal_A);
	printf("Det finns %d drag av kategori B",antal_B);
}
/*
0 = tom ruta
1 = vit pj�s
2 = svart pj�s

Kategori A-drag: Rakt upp�t om rutan �r tom.
Kategori B-drag: Snett upp�t �t h�ger/v�nster om det finns en svart pj�s i dest-rutan.
*/
