#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define STORLEK 200

int tab[STORLEK][STORLEK];
int mem[STORLEK][STORLEK];
int max_s=0;
clock_t start,end;
double elapsed = 0;
int quit = 0;
int n; //antal niv�er/rader i tr�det

int max(int a, int b) {
    if (a>b) {
        return a;
    } else {
        return b;
    }
}

int solve(int rad, int kol) {
//    if (quit == 1) return;
//    end = clock();
//    elapsed = ((double) (end - start)) / CLOCKS_PER_SEC;
//    if (elapsed >= 30 && quit == 0) {
//        printf("Antal nivaer: %d \nTid: %f \n",rad,elapsed);
//        quit = 1;
//        return;
//    }
    
    if (mem[rad][kol] != -1) {
        return mem[rad][kol]; //om alla valm�jligheter finns lagrade, returnera
    }
    if(rad==n-1) {
        return tab[rad][kol]; 
    } else {
        int a,b;
        a = solve(rad+1,kol);
        b = solve(rad+1,kol+1);
        //lagra nuvarande radens summa + n�sta rads h�gsta valm�jlighet
        mem[rad][kol] = tab[rad][kol]+max(a,b);
        return mem[rad][kol];
    }
}

void readFile() {
    FILE *infil;
    int i,j;
    infil=fopen("input.dat","rt");
    fscanf(infil,"%d",&n);
    for(i=0;i<n;i++)
        for(j=0;j<=i;j++)
            fscanf(infil,"%d",&tab[i][j]);
    fclose(infil);
}

int main(void){
    readFile();
    
    int i,j;
//    n = STORLEK;
//    for(i=0;i<STORLEK;i++) {
//        for(j=0;j<=i;j++) {
//            tab[i][j]=rand()%100; //fyll tr�det med slumptal
//        }
//    }
    for(i=0;i<STORLEK;i++) {
        for(j=0;j<STORLEK;j++) {
            mem[i][j] = -1; //nollst�ll cache-matrisen
        }
    }
    
//    start = clock();
    printf("Maximal summa: %d",solve(0,0));
    
//    printf("Maximal summa: %d",max_s);
    return 0;
}
