#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

int min (int a, int b) {
    if (a < b) {
        return a;
    } else {
        return b;
    }
}

int main(void) {
	int a[6][6]={{1000, 3, 7, 8,1000,1000},
                {1000,1000, 4,1000, 9,1000},
                {1000,1000,1000, 2, 6,1000},
                {1000,1000,1000,1000,1000, 6},
                {1000,1000,1000,1000,1000, 4},
                {1000,1000,1000,1000,1000,1000}};
    int i,j;
    int d[6]={0,1000,1000,1000,1000,1000};
    for(i=0;i<6;i++) {
        for(j=0;j<i;j++) {
            d[i]=min(d[i],d[j]+a[j][i]);
            printf("d[%d]=%d \n",i,d[i]);
        }
    }
    
	return 0;
}
