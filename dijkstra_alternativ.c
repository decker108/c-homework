#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

//graph[][] = graf-arrayen med st�derna
//roadCount = antal v�gar
//nodeCount = antal st�der
//start = startpos
//stop = slutpos
//path[] = lagrar kortaste v�gen fr�n start till staden med indexnumret
void dijkstra(int graph[][3],int roadCount,int nodeCount,int start,int stop,int path[]) {
    int t[10],i,j,v,x,min;
    for(i=0;i<=0+nodeCount;i++) {
        path[i] = INT_MAX;
        t[i] = 1;
    }
    path[start] = 0;
    while(t[stop]) {
        min = INT_MAX;
        for(i=0;i<=0+nodeCount;i++) {
            if(t[i] && path[i]<min) {
                v = i;
                min = path[i];
            }
        }
        t[v] = 0;
        for(j=0;j<roadCount;j++) {
            x = -1;
            if(graph[j][0]==v) {
                x = graph[j][1];
            }
            if(graph[j][1]==v) {
                x = graph[j][0];
            }
            if(x>=0 && path[x]>path[v]+graph[j][2]) {
                path[x] = path[v]+graph[j][2];
            }
        }
    }
}

int main(void) {
    int graph[][3]= {{0,1,2},{0,5,1},{1,3,2},{3,5,1},
                     {1,2,2},{1,4,4},{2,4,3},{2,7,1},
                     {3,4,4},{4,6,7},{6,7,6},{5,6,5}};
    int path[10];
    dijkstra(graph,12,8,0,7,path);
    printf("Shortest part: %d\n",path[7]);
    
    return 0;
}
