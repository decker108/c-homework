#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

struct objekt{
	int tal;
	struct objekt *left,*right;
};
typedef struct objekt objekt;

objekt *theQueue[100] = {NULL};

objekt *InsertTree(objekt *p,objekt ny){
	objekt *z;

	if(p==NULL){
		z=(objekt *) malloc(sizeof(objekt));
		*z=ny;
		z->left=NULL;
		z->right=NULL;
		return z;
	} else {
		if(ny.tal<p->tal) {
			p->left=InsertTree(p->left,ny);
		} else {
			p->right=InsertTree(p->right,ny);
		}
	}
	return p;
}

objekt *Init(void){
	objekt p,*start;
	int i,t,n;
	FILE *infil;

	infil=fopen("uppg5.dat","rt");
	fscanf(infil,"%d",&n);
	start=NULL;
	for(i=1; i<=n; i++){
		fscanf(infil,"%d",&t);
		p.tal=t;
		p.right=NULL;
		p.left=NULL;
		start=InsertTree(start,p);
	}
	fclose(infil);
	return start;
}

void DeleteTree(objekt **x) {
	objekt *z;

	if (*x != NULL) {
		if ((*x)->right == NULL) {
			z = *x;
			*x = (*x)->left;
			free(z);
		} else if ((*x)->left == NULL) {
			z = *x;
			*x = (*x)->right;
			free(z);
		} else {
			z = (*x)->right;
			while (z->left != NULL) {
				z = z->left;
			}
			z->left = (*x)->left;
			z = *x;
			*x = (*x)->right;
			free(z);
		}
	}
}

bool queueIsEmpty(int i) {
	if (theQueue[i] == NULL) {
		return true;
	} else {
		return false;
	}
}

/**
 * Anv�nder bfs f�r att skriva ut nycklarna niv� f�r niv�
 */
void PrintTreeLevel(objekt *start) {
	objekt *current;
	current = start;
	int front = 0, rear = 0;
	theQueue[front] = current;
	while (true) {
		if (queueIsEmpty(front) == true) { //�r k�n tom?
			break;
		}
		current = theQueue[front]; //plocka ut f�rsta elementet
		printf("%d ",current->tal);
		if (current->left != NULL) { //om det finns elem till v�nster...
			rear++; //stega ins�ttnings-index
			theQueue[rear] = current->left; //l�gg till pekare i k�n
		}
		if (current->right != NULL) { //om det finns elem till h�ger...
			rear++;
			theQueue[rear] = current->right;
		}
		theQueue[front] = NULL; //t�m nuvarande plats
		front++; //stega fram uttags-index
	}
	DeleteTree(&start);
}

int main(void){
	objekt *start;

	start = Init();
	PrintTreeLevel(start);
}
