//CL060111 Uppgift 4 Personregistret
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct persnrtyp {
    char persnr[16];
    int postnr;
};
struct namntyp {
    char namn[32];
};

int main(void) {
	//variabler
	struct persnrtyp nummer;
	struct namntyp personnamn;
	char input_persnr[16];
	FILE *id_fil,*namn_fil;

	printf("Personnummer: ");
	scanf("%s",input_persnr);

	id_fil=fopen("persnr.dat","rb");
	namn_fil=fopen("namn.dat","rb");

    int error_flagga=1;
	while(!feof(id_fil)){
        fread(&nummer,sizeof(struct persnrtyp),1,id_fil);
        int jmf=strcmp(nummer.persnr,input_persnr); //nummer.persnr==input_persnr
        if (jmf==0) {
            error_flagga=0;
            break;
        }
	}
	if (error_flagga==1) {
	    printf("Personnumret ej funnet.");
	}

    if (error_flagga!=1) {
        fseek(namn_fil,(nummer.postnr*sizeof(struct namntyp)),SEEK_SET);
        fread(&personnamn,sizeof(struct namntyp),1,namn_fil);
        printf("Personen heter: %s",personnamn.namn);
        fclose(id_fil);
        fclose(namn_fil);
    }
}
/*
Exempel: 8408158744

1. L�s in person-nr.
2. Hitta person-nr i persnr.dat och spara post-nr.
3. G� till posten med angivet post-nr i namn.dat och skriv ut namnet.

s=sizeof(ont);
fseek(infil,10*s,SEEK_SET);
fread(...);
*/
