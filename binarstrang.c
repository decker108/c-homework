//CL051021 Uppgift 6 Bin�rstr�ng
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void utskrift (int radnr, char * strang);
int main(void) {
	//variabler
	char binstring[12]; //enl uppgift 80 (81)

	printf("Skriv in en bin�rstr�ng: ");
	scanf("%s",binstring);

    //ber�kna l�ngd p� bin�rstr�ngen
	int antal_siffror=strlen(binstring);

	utskrift(0,binstring); //skriv ut rad 0 (den inmatade raden)
	int n; //antalet siffror man vill producera p� varje rad
    //antalet siffror p� varje rad = ursprungstalet-1-(1/rad). Loopen slutar n�r n=0
	for (n=antal_siffror-1; n>0; n--) {
        int i;
        for (i=0; i<n; i++) {
            //antalet siffror som behandlas minskar med 1 varje rad
            binstring[i]=(binstring[i]==binstring[i+1]) ? '0' : '1';
        }
        binstring[n]='\0'; //det sista tecknet blir sluttecken (n �r utanf�r loopen)
        utskrift(antal_siffror-n,binstring); //skriv ut en rad
	}
	printf("Det sista tecknet blir %s",binstring);
}

void utskrift (int radnr, char * strang) { //*strang pekar p� plats 0 i binstring
    int a;
    for (a=0; a<radnr; a++) {
    	printf(" "); //antalet mellanrum v�xer f�r varje rad
    }
    while (* strang) { //s� l�nge binstring inte �r NULL k�rs loopen
        printf("%c ",* strang); //skriv ut 1 tecken+mellanslag fr�n binstring
        strang++; //stega fram�t i binstring
    }
    printf("\n"); //n�r alla tecken skrivits ut, ta ny rad
}
/*
1011001010

regler f�r nya tecken:
1. om siffrorna ovan �r lika = 0
2. om siffrorna ovan �r olika = 1
*/
