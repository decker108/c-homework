/*
Stigande siffror, CL040413 uppgift 1
Ola Rende, DP09
*/
#include <stdlib.h>
#include <stdio.h>
int main(void){
    int tal,undre,ovre,testtal;
    int antalstigande = 0;
    printf("Ange undre gr�ns: ");
    scanf("%d",&undre);
    printf("Ange �vre gr�ns: ");
    scanf("%d",&ovre);
    for (tal=undre; tal<=ovre; tal++) {
        testtal=tal;
        int s4=testtal%10; testtal=testtal/10; //
        int s3=testtal%10; testtal=testtal/10; //
        int s2=testtal%10; testtal=testtal/10; //
        int s1=testtal%10; testtal=testtal/10; //
        if (s1<s2 && s2<s3 && s3<s4) {
            antalstigande++;
        }
    }
    printf("I intervallet finns %d st tal med stigande sifferv�rden.",antalstigande);
}
