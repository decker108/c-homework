#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

int lista[50] = {0};
int mynt[8] = {1,5,10,20,50,100,500,1000};
unsigned long int anrop = 0;

int change2(int belopp) {
    anrop++;
    if (lista[belopp] != 0) {
        return lista[belopp];
    }
    int min = belopp;
    int i;
    for (i=0; i<8; i++) {
    	if (mynt[i] == belopp) {
    	    return 1;
    	}
    }
    for (i=1; i<belopp/2; i++) {
        int n = change2(i)+change2(belopp-i);
    	if (n<min) {
    	    min = n;
    	}
    }
    lista[belopp] = min;
    return min;
}

int change(int belopp) {
    anrop++;
    int min = belopp;
    int i;
    for (i=0; i<8; i++) {
    	if (mynt[i] == belopp) {
    	    return 1;
    	}
    }
    for (i=1; i<belopp/2; i++) {
        int n = change(i)+change(belopp-i);
    	if (n<min) {
    	    min = n;
    	}
    }
    return min;
}

int main(void) {
    int n = 29;
	printf("change(%d)=%d (anrop=%ld) \n",n,change(n),anrop);
	anrop = 0;
	printf("change2(%d)=%d (anrop=%ld) \n",n,change2(n),anrop);
	
	return 0;
}
