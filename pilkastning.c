//CL050315 Uppg 4 Pilkastning
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main(void) {
	//variabler
	int slumptal;
	float totalpoang=0.0;
	srand(time(0));

	//slumpfunktion mellan 1-100 (%99+1)
        //g�r fem slumptal, kolla upp mot villkoren, sparar po�ngen
        //upprepa 10000 ggr
    for (int a=0; a<=10000; a++) {
        int poang=0;
        for (int b=0; b<=4; b++) {
            slumptal=rand()%99+1;
            if (slumptal==1 || slumptal==2) { //2% 1-2
                poang+=6;
            }
            else if (slumptal>2 && slumptal<=7) { //5% 3,4,5,6,7
                poang+=5;
            }
            else if (slumptal>7 && slumptal<=17) { //10% 8,9,10,11,12,13,14,15,16,17
                poang+=4;
            }
            else if (slumptal>17 && slumptal<=34) { //17% 18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34
                poang+=3;
            }
            else if (slumptal>34 && slumptal<=62) { //27% 35-62
                poang+=2;
            }
            else if (slumptal>62) { //39%
                poang+=1;
            }
        }
        totalpoang=totalpoang+poang;
    }
    //ber�kna medelv�rdet av totalpo�ngen/antal_spel
    printf("Kalles po�ng blir %.2f",totalpoang/10000.0);
}
