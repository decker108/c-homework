#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

struct objekt{
    char nyckel[10];
    struct objekt *left,*right;
};
typedef struct objekt objekt;

void postorder(objekt *start){
    objekt *x;
    x = start;
    if(x != NULL){
        postorder(x->left);
        postorder(x->right);
        printf("%s ",x->nyckel);
    }
}

void inorder(objekt *start) {
    objekt *x;
    x = start;
    if(x != NULL){
        inorder(x->left);
        printf("%s ",x->nyckel);
        inorder(x->right);
    }
}

void preorder(objekt *start) {
    objekt *x;
    x = start;
    if(x != NULL){
        printf("%s ",x->nyckel);
        preorder(x->left);
        preorder(x->right);
    }
}

int main(void) {
	
	return 0;
}