//CL091019 Uppg 5 Brickspel
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main(void) {
    //variabler
    char spelplan[6];
    char spelpjas;
    int fran,till;
    int antal_drag;
    FILE *fil;
    fil=fopen("drag.txt","rt");
    //fil=fopen("drag2.txt","rt");
    //fil=fopen("drag3.txt","rt");

    fscanf(fil,"%s",spelplan);
    fscanf(fil,"%d",&antal_drag);

    int i;
    for (i=0; i<antal_drag; i++) {
    	fscanf(fil,"%d %d",&fran,&till); //l�s in ett drag, best�m index
    	fran--; //justera siffror fr�n 1-10 till 0-9
    	till--;
    	int index=fran; //Index inneh�ller den nuvarande positionen p� spelplanen
    	//kolla riktning
    	if (till<fran) { //�t v�nster
    	    while (1) {
                if (index==fran) {
                    spelpjas=spelplan[index];
                    spelplan[index]='T';
                    index--;
                }
                if (index==till) {
                    spelplan[index]=spelpjas;
                    break;
                }
                if (spelplan[index]=='S') {
                    spelplan[index]='V';
                    index--;
                } else if (spelplan[index]=='V') {
                    spelplan[index]='S';
                    index--;
                }
    	    }
    	} else if (till>fran) { //�t h�ger
            while (1) {
                if (index==fran) {
                    spelpjas=spelplan[index];
                    spelplan[index]='T';
                    index++;
                }
                if (index==till) {
                    spelplan[index]=spelpjas;
                    break;
                }
                if (spelplan[index]=='S') {
                    spelplan[index]='V';
                    index++;
                } else if (spelplan[index]=='V') {
                    spelplan[index]='S';
                    index++;
                }
    	    }
    	}
    }
    //st�ng filstr�m, skriv resultat
    fclose(fil);
    printf("Slutst�llning: %s",spelplan);
}
/*
5 rutor: |1|2|3|4|5| ( |0|1|2|3|4| )
4 pj�ser med svarta och vita sidor.
Alla drag representerar rutor p� br�det, men m�ste subtraheras med -1.
T=tom, S=svart, V=vit
*/
