//F1 Uppg 1.5
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int antal = 0;
int v = 0;

int alg(int m, int n) {
    antal++;
    int v = 0;
    if (m == 0 && n == 0) {
        return 1;
    } else {
        if (m>0) {
            v = v + alg(m-1,n);
        }
        if (n>0) {
            v = v + alg(m,n-1);
        }
        return v;
    }
}

int main(int argc, char* args[]) {
    int rad,kol;
	for (rad=1; rad<10; rad++) {
		for (kol=1; kol<10; kol++) {
			printf("Antal vagar(%d,%d): %6d  (antal anrop=%d)\n",rad,kol,alg(rad,kol),antal);
			antal = 0;
		}
	}
	exit(EXIT_SUCCESS);
}

