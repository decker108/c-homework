#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int spelvinst(int inkomst_per_tomruta) {
     // g�r ett spel
    int bollkast;
    int atraff=0,btraff=0,ctraff=0,dtraff=0,tomruta=0;
    for (int b=1; b<=5; b++) {
        bollkast=rand()%4; //Slumptalet kan bli 0,1,2,3
        switch (bollkast){
            case 0: atraff++; break;
            case 1: btraff++; break;
            case 2: ctraff++; break;
            case 3: dtraff++;
        }
    }
    // r�kna tomma rutor
    int antal_tomma=0;
    if (atraff==0) {
        antal_tomma++;
    }
    if (btraff==0) {
        antal_tomma++;
    }
    if (ctraff==0) {
        antal_tomma++;
    }
    if (dtraff==0) {
        antal_tomma++;
    }
    // returnera vinst
    return inkomst_per_tomruta*antal_tomma-10;
}

int main(void){
    int inkomst_per_tomruta;
    int antal_spel=10000;
    int total_vinst=0;
    srand(time(0));
    printf("Hur mycket f�r man per tom ruta? ");
    scanf("%d",&inkomst_per_tomruta);
    for (int a=1; a<=antal_spel; a++) {
        total_vinst += spelvinst(inkomst_per_tomruta);
    }
    float medelvinst = ((float)total_vinst)/antal_spel;
    printf("Spelaren vinner: %.2f kr",medelvinst);
    }

