#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

struct objektTyp {
    char nyckel[15];
    struct objektTyp *left, *right;
};
typedef struct objektTyp objekt;

objekt *SearchTree(objekt *x,char namn[]){
    if(x == NULL) {
        return NULL;
    }
    if(x != NULL && strcmp(x->nyckel,namn) == 0) {
        return x;
    }
    if(x != NULL) {
        if (strcmp(namn,x->nyckel) < 0) {
            x = SearchTree(x->left,namn);
        } else {
            x = SearchTree(x->right,namn);
        }
    }
    return x;
}

objekt *InsertTree(objekt *p, char* ny){
    objekt *z;
    if(p == NULL){
        z = new objekt;
        strcpy(z->nyckel,ny);
        z->left = NULL;
        z->right = NULL;
        return z;
    } else {
        if(strcmp(ny, p->nyckel) < 0) {
            p->left = InsertTree(p->left,ny);
        } else {
            p->right = InsertTree(p->right,ny);
        }
        return p;
    }
}

objekt *InsertTree2(objekt *start, char* ny){
    objekt *x,*y,*z;
    z = (objekt *) malloc(sizeof(objekt));
    strcpy(z->nyckel, ny);
    z->left = NULL;
    z->right = NULL;
    y = NULL;
    x = start;
    while(x != NULL){
        y = x;
        if (strcmp(z->nyckel,x->nyckel) < 0) {
            x = x->left;
        } else {
            x = x->right;
        }
    }
    if(y == NULL)
        start = z;
    else {
        if(strcmp(z->nyckel,y->nyckel) < 0) {
            y->left = z;
        } else {
            y->right = z;
        }
    }
    return start;
}

int main(void) {
	//inte klar
	return 0;
}