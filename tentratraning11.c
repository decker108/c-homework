//tentatr�ning problem 11
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int match(char *pattern, char *word) {
    while (*pattern && *word) { //s� l�nge orden inte tagit slut...
        if (*pattern!='*') { //om inputordets tecken inte �r en *...
            if (*pattern!=*word) { //om orden inte inneh�ller samma tecken...
                return 0; //mismatch!
            }
        } //annars, g� till n�sta tecken och g�r nytt varv.
        pattern++;
        word++;
    }
    return 1; //om alla tecken (f�rutom *) st�mde �verens �r det match.
}

int main(void) {
    char input[6],sokord[6];
    int antal_ord;

    printf("Skriv in ord p� 5 bokst�ver med minst 1 st *: ");
    scanf("%s",input);

	FILE *fil;
	fil=fopen("Uppg11.dat","rt");
	fscanf(fil,"%d",&antal_ord);

	int i;
	for (i=0; i<antal_ord; i++) {
		fgets(sokord,10,fil);
		int test = match(input,sokord);
		if (test==1) {
		    printf("%s ",sokord);
		}
	}
	fclose(fil);
}
