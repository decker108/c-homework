//CL050307 Uppgift 7 Befolkningst�thet
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
struct land{
    char namn[40];
    int invantal;
    float yta;
};
int main(void) {
    struct land landsdata[192];
    char temp_namn[40],maxnamn[40];
    const int varvtal=191;
    float temp_yta,folktathet,max=0;
    FILE *fil_a,*fil_b;

    //l�s in alla landsnamn och folkm�ngder i struct'en
    fil_a=fopen("050307-7folkmangd.txt","rt");
    for (int a=0; a<=varvtal; a++) {
    	fscanf(fil_a, "%s", landsdata[a].namn);     //l�s in landets namn
    	fscanf(fil_a, "%d", &landsdata[a].invantal); //l�s in landets inv�narantal
    }
    fclose(fil_a);

    //l�s in alla ytor till resp r�tt plats i struct'en
    fil_b=fopen("050307-7storlek.txt","rt");
    for (int a=0; a<=varvtal; a++) {
    	fscanf(fil_b, "%s", temp_namn);
    	fscanf(fil_b, "%f", &temp_yta);
    	for (int a=0; a<=varvtal; a++) {
    	    int jmf = strcmp(temp_namn,landsdata[a].namn);
    	    if (jmf==0) {
                landsdata[a].yta=temp_yta;
    	    }
    	}
    }
    fclose(fil_b);

    //printf("%s %d %f",landsdata[0].namn,landsdata[0].invantal,landsdata[0].yta);

    //ber�kna befolkningst�thet och spara maximum
    for (int a=0; a<=varvtal; a++) {
    	folktathet=(float)landsdata[a].invantal/landsdata[a].yta;
    	if (folktathet>max) {
            max=folktathet;
            strcpy(maxnamn,landsdata[a].namn);
        }
    }
    //skriv ut svaret
    printf("I %s bor folkt�tast med %.0f personer/km^2",maxnamn,max);
}
/*
1. L�s in landsnamn & inv�narantal i 192 struct'er.
2. L�s igenom landsnamn, j�mf�r med struct, l�s in yta f�r r�tt land.
3. G� igenom struct'en, ber�kna befolkningst�thet, spara maximum-tal och namn.
*/
