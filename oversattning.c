//CL040413 Uppg 6 �vers�ttning
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct lexikon{
    char svenska[11];
    char engelska[11];
};
struct lexikon post[20];

void oversatt_ord(char *token);

void oversatt_mening (char *input){
    char *token=strtok(input," ");
    while (token!=NULL) {
        //printf("%s \n",token);
        oversatt_ord(token);
        token=strtok(NULL," ");
    }
}

void oversatt_ord (char *token) {
    for (int a=0; a<20; a++) {
    	int jmf=strcmp(token,post[a].svenska);
    	if(jmf==0) {
    	    printf("%s ",post[a].engelska);
    	}
    }
}

int main(void) {
	//variabler
	char input_sv[80];
	int antal_rader;
	FILE *fil;

	//skriv in mening p� svenska, max 80 tecken
	printf("Skriv in en mening p� svenska: ");
	gets(input_sv);


	//�ppna filen och l�s antalet rader
	fil=fopen("ordbok.txt","rt");
	fscanf(fil,"%d",&antal_rader);

    //l�s in lexikonet i structen
    int index=0;
    while (!feof(fil)) {
        fscanf(fil,"%s",post[index].svenska);
        fscanf(fil,"%s",post[index].engelska);
        index++;
    }

    //dela upp meningen efter mellanslagen och l�gg in i struct-poster
    //�vers�tt ord f�r ord och l�gg till eng-orden till utskrifts-str�ngen
    oversatt_mening(input_sv);

    //resultatet skrivs ut i funktion
}
