#include <stdio.h>
int m[16][2]={  {2,8},{5,10},{7,3},{11,0},{13,10},{17,-2},
                {18,3},{21,4},{28,-5},{31,7},{35,9},
                {37,2},{42,6},{53,15},{58,12},{64,5}};

//avst�ndsformeln
float avst(int x1,int y1,int x2,int y2){
    return sqrt(pow(x1-x2,2)+pow(y1-y2,2));
}

//n�rmaste tv� punkterna (divide & conquer)
float closestpair(int s,int t){
    float d,d1,d2,d3,mitt,v,h;
    int i,j;
    if(t-s==1) {
        return avst(m[t][0],m[t][1],m[s][0],m[s][1]);
    } else {
        d1 = closestpair(s,(s+t-1)/2);
        d2 = closestpair((s+t-1)/2+1,t);
        mitt=(m[(s+t-1)/2][0]+m[(s+t-1)/2+1][0])/2;
        if(d1<d2) { 
            d = d1; 
        } else { 
            d = d2; 
        }
        v = mitt - d;
        h = mitt + d;
        for(i=(s+t-1)/2;i>=s;i--) {
            if(m[i][0]>v) {
                for(j=(s+t-1)/2+1;j<=t;j++) {
                    if(m[j][0]<h) {
                        d3 = avst(m[i][0],m[i][1],m[j][0],m[j][1]);
                        if(d3<d) d=d3;
                    }
                }
            }
        }
        return d;
    }
}

int main(void) {
    printf("%.2f\n",closestpair(1,16));
}