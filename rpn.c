// Kapitel 4.1.13 - Ber�kning av artimetiska uttryck (aka reverse polish notation)
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include <string.h>

char stack[81];
float stack2[41];
int antal=0,antal2=0;

int prio(char c){
    switch (c){
        case '*': return 2; break;
        case '/': return 2; break;
        case '+': return 1; break;
        case '-': return 1; break;
        case '(': return 0; break;
    }
}

void push(char x){
    antal++;
    stack[antal]=x;
}

char pop(void){
    return (stack[antal--]);
}

char top(void){
    return (stack[antal]);
}

int tom(void){
    return antal==0;
}

void strchar(char s[ ],char c){
    int p;
    p=strlen(s);
    s[p]=c;
    s[p+1]='\0';
}

void konvertera(char postfix[],char infix[]){
    int k,klar;
    char c;
    for(k=0;k<strlen(infix);k++){
        c=infix[k];
        switch (c){
            case '0': case '1':
            case '2': case '3':
            case '4': case '5':
            case '6': case '7':
            case '8': case '9': strchar(postfix,c); break;
            case '*': case '/':
            case '+': case '-' : {
                klar=0;
                do{
                    if(tom())
                        klar=1;
                    else{
                        if(prio(c)<=prio(top()))
                            strchar(postfix,pop());
                        else
                            klar=1;
                    }
                }while(!klar);
                push(c);
            }; break;
            case '(': push(c); break;
            case ')':
            while (stack[antal]!='(')
            strchar(postfix,pop());
            pop();break;
        }
    }
    while(!tom())
    strchar(postfix,pop());
}

void push2(float x){
    antal2++;
    stack2[antal2]=x;
}

float pop2(void){
    return stack2[antal2--];
}

float berakna(char postfix[]){
    int k,t1,t2;
    char c;
    for(k=0;k<strlen(postfix);k++){
        c=postfix[k];
        switch (c){
                case '0': 
                case '1': 
                case '2': 
                case '3':
                case '4': 
                case '5': 
                case '6': 
                case '7':
                case '8': 
                case '9': push2(c-48); break;
                case '*': push2(pop2()*pop2()); break;
                case '+': push2(pop2()+pop2()); break;
                case '/': push2(1.0/pop2()*pop2()); break;
                case '-': {
                    t1=pop2();
                    t2=pop2();
                    push2(t2-t1);
                } break;
        }
    }
    return pop2();
}

int main(void){
    char postfix[80]="",infix[80];
    do{
        infix[0]='\0';
        printf(": ");
        gets(infix);
        if(strlen(infix)>0){
            konvertera(postfix,infix);
            printf("%s\n",postfix);
            printf("%.4f\n",berakna(postfix));
        }
    }while(strlen(infix)!=0);
    return 0;
}
