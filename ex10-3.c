#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include <time.h>
#include <string.h>

void mergesort(int a[], int n, int temp[]) {
    int i1, i2, tempi;
    if (n == 1) {
    	return;
    }
    if (n == 2) {
    	int t = a[0];
    	a[0] = a[1];
    	a[1] = t;
    }

    mergesort(a, n/2, temp); //halvera listan
    mergesort(a + n/2, n - n/2, temp); //???
    i1 = 0; //b�rjan
    i2 = n/2; //mitten
    tempi = 0; //b�rjan av temp-arrayen
    //loopar fr�n start till mitten OCH mitten till slutet
    while (i1 < n/2 && i2 < n) { 
        if (a[i1] < a[i2]) {
            temp[tempi] = a[i1];
            i1++;
        } else {
            temp[tempi] = a[i2];
            i2++;
        }
        tempi++;
    }

    while (i1 < n/2) {
        temp[tempi] = a[i1];
        i1++;
        tempi++;
    }
    while (i2 < n) {
        temp[tempi] = a[i2];
        i2++;
        tempi++;
    }

    memcpy(a, temp, n*sizeof(int));
}

int main(void) {
    clock_t startTime, stopTime, startTime2, stopTime2;
    srand(time(0));
    int n = 100000;
    int* x = malloc(sizeof(int)*n);
    int* x2 = malloc(sizeof(int)*n);
    int* temp = malloc(sizeof(int)*n);
    int i;
    for (i=0; i<n; i++) {
        int randNum = rand()%n+1;
    	x[i] = randNum;
    	x2[i] = randNum;
    }
    startTime = clock()/CLK_TCK;
	mergesort(x,n,temp);
	stopTime = clock()/CLK_TCK;
	
	startTime2 = clock()/CLK_TCK;
	int k,m,tmp;
    for (k=0; k<n-1; k++) {
        for (m=k+1; m<n; m++) {
            if (x2[k]>x2[m]) {
                tmp=x2[k];
                x2[k]=x2[m];
                x2[m]=tmp;
            }
        }
    }
	stopTime2 = clock()/CLK_TCK;
	
	for (i=0; i<n; i++) {
	    if (i!=0 && x2[i]<x2[i-1] && x[i]<x[i-1]) {
	        printf("ERROR");
	        break;
	    }
//	    if (i%25==0 && i!=0) {
//    	    printf("\n");
//    	}
//    	printf("%4d ",x2[i]);
    }
    
    printf("\nMergesort tid: %f sek \nBubblesort tid: %f sek",
    (double)((stopTime-startTime)),
    (double)((stopTime2-startTime2)));
    return 0;
}
