#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

unsigned int cache[40];

unsigned int funk(int n) {
    if (cache[n] != 0 || n == 0 || n == 1) {
        return cache[n];
    }
	unsigned int a = funk(n-1);
	cache[n-1] = a;
	unsigned int b = funk(n-2);
	cache[n-2] = b;
	unsigned int c = funk(n-3);
	cache[n-3] = c;
	return a+b+c;
}

int main(void) {
    int n,i;
    for (i=0; i<40; i++) {
    	cache[i] = 0;
    }
    cache[2] = 1;
	for (n=0; n<39; n++) {
		printf("t(%d)=%d \n",n,funk(n));
	}
	return 0;
}
