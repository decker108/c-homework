#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <assert.h>

#define EMPTY 0
#define BLACK 1
#define WHITE 2

#define LEN 6

char strForm[LEN] = {"%d-%d "};

void removeLast4(char* str) {
    int i = 0;
    while (str[i] != '\0') {
        i++;
    }
    str[i-(LEN-2)] = '\0';
}

int victory(int testboard[7]) {
    int i;
    for (i=0; i<3; i++) {
    	if (!(testboard[i] == WHITE && testboard[i+4] == BLACK)) {
    	    return 0;
    	}
    }
    return 1;
}

void solve(int board[7], int turns, char* str) {
    if (victory(board) == 1) {
        printf("M\x86let n\x86tt efter %d steg \n",turns);
        printf("%s \n",str);
    } else {
        int i;
        for (i=0; i<7; i++) {
            int temp = board[i];
            char str2[LEN];
            if (board[i] == BLACK) {
                if (board[i+1] == EMPTY) {
                    board[i+1] = temp;
                    board[i] = EMPTY;
                    sprintf(str2,strForm,(i)+1,(i+1)+1);
                    strcat(str,str2);
                    solve(board,turns+1,str);
                    removeLast4(str); //exp
                    board[i] = temp;
                    board[i+1] = EMPTY;
                }
                if (board[i+2] == EMPTY) {
                    board[i+2] = temp;
                    board[i] = EMPTY;
                    sprintf(str2,strForm,(i)+1,(i+2)+1);
                    strcat(str,str2);
                    solve(board,turns+1,str);
                    removeLast4(str); //exp
                    board[i] = temp;
                    board[i+2] = EMPTY;
                }
            }
            if (board[i] == WHITE) {
                if (board[i-1] == EMPTY) {
                    board[i-1] = temp;
                    board[i] = EMPTY;
                    sprintf(str2,strForm,(i)+1,(i-1)+1);
                    strcat(str,str2);
                    solve(board,turns+1,str);
                    removeLast4(str); //exp
                    board[i] = temp;
                    board[i-1] = EMPTY;
                }
                if (board[i-2] == EMPTY) {
                    board[i-2] = temp;
                    board[i] = EMPTY;
                    sprintf(str2,strForm,(i)+1,(i-2)+1);
                    strcat(str,str2);
                    solve(board,turns+1,str);
                    removeLast4(str); //exp
                    board[i] = temp;
                    board[i-2] = EMPTY;
                }
            }
        }
    }
}

int main(void) {
	int initboard[7] = {BLACK,BLACK,BLACK,0,WHITE,WHITE,WHITE};
	char str[50] = {""};
	solve(initboard,0,str);
	return 0;
}
