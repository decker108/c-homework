#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

#define antal 8

int lcs(int a[],int N){
    int *best,*prev,i,j,max=0;
    best=(int*)malloc(sizeof(int)*N);
    prev=(int*)malloc(sizeof(int)*N);
    for(i=0;i<N;i++) {
        best[i] = 1;
        prev[i] = i;
    }
    for(i=1;i<N;i++) {
        for(j=0;j<i;j++) {
            if(a[i]>a[j] && best[i]<best[j]+1) {
                best[i]=best[j]+1,prev[i]=j;
            }
        }
    }
    for(i=0;i<N;i++) {
        if(max<best[i]) {
            max=best[i];
        }
    }
    free(best);
    free(prev);
    return max;
}

int main(void) {
    int a[antal] = {5,2,8,6,3,6,9,7};
	printf("lcs = %d",lcs(a,antal));
	
	return 0;
}

