//CL050825 Uppg 6 Flygfotot
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int husnr=1;
const int maxstorlek=21;
char flygfoto[maxstorlek+1][maxstorlek+1];
int jmf[maxstorlek+1][maxstorlek+1]={0};

//returnerar befintligt husnr p� omkringliggande rutor eller 0 om det ej hittas.
int hitta_befintligt_husnr(int rad, int kolumn){
    if (flygfoto[rad][kolumn]=='x' && jmf[rad][kolumn]!=0){ //titta p� mitten-rutan
        return jmf[rad][kolumn];
    }
    if (flygfoto[rad][kolumn+1]=='x' && jmf[rad][kolumn+1]!=0){ //titta till h�ger
        return jmf[rad][kolumn+1];
    }
    if (flygfoto[rad][kolumn-1]=='x' && jmf[rad][kolumn-1]!=0){ //titta till v�nster
        return jmf[rad][kolumn-1];
    }
    if (flygfoto[rad+1][kolumn]=='x' && jmf[rad+1][kolumn]!=0){ //titta ovanf�r
        return jmf[rad+1][kolumn];
    }
    if (flygfoto[rad-1][kolumn]=='x' && jmf[rad-1][kolumn]!=0){ //titta nedanf�r
        return jmf[rad-1][kolumn];
    }
    return 0;
}

//s�tt husnr p� alla celler med x
void satt_husnr (int rad, int kolumn, int husnr){
    if (flygfoto[rad][kolumn]=='x'){ //titta p� mitten-rutan
        jmf[rad][kolumn]=husnr; //markera med nuvarande husnr
    }
    if (flygfoto[rad][kolumn+1]=='x'){ //titta till h�ger
        jmf[rad][kolumn+1]=husnr;
    }
    if (flygfoto[rad][kolumn-1]=='x'){ //titta till v�nster
        jmf[rad][kolumn-1]=husnr;
    }
    if (flygfoto[rad+1][kolumn]=='x'){ //titta ovanf�r
        jmf[rad+1][kolumn]=husnr;
    }
    if (flygfoto[rad-1][kolumn]=='x'){ //titta nedanf�r
        jmf[rad-1][kolumn]=husnr;
    }
}

int main(void) {
	//variabler
	int antal_rader,antal_tecken;
	FILE *fil;

    //�ppna fil, l�s antal rader och antal tecken
	fil=fopen("flygfoto.txt","rt");
	//fil=fopen("050825-6.txt","rt"); //alternativ redovisningsfil
	fscanf(fil,"%d",&antal_rader);
	fscanf(fil,"%d",&antal_tecken);

    //l�s in fil till matris-str�ng
	for (int rad=0; rad<antal_rader; rad++) {
		fscanf(fil,"%s",&flygfoto[rad][1]); //b�rja p� ruta 1 ist f�r 0.
		printf("%s\n",&flygfoto[rad][1]);
        flygfoto[rad][0]='.'; //ruta 0 tilldelas punkter.
	}
    printf("\n");

	//titta efter hus (x-tecken) i x-led och r�kna ihop dem till antal hus.
	for (int rad=0; rad<=maxstorlek; rad++) {
        for (int kolumn=0; kolumn<=maxstorlek; kolumn++) {
        	//om ett 'x' hittas, titta i rutorna runtomkring
        	if (flygfoto[rad][kolumn]=='x') {
        	    int befintligt_husnr=hitta_befintligt_husnr(rad,kolumn);
                if (befintligt_husnr!=0) {
                    satt_husnr(rad,kolumn,befintligt_husnr);
                    //printf("%d \n",befintligt_husnr);
                } else {
                    //skapa ett nytt husnr
                    satt_husnr(rad,kolumn,husnr);
                    //printf("%d ",husnr);
                    husnr++;
                }
        	}
        }
	}

	for (int rad=0; rad<12; rad++) {
	    for (int kolumn=0; kolumn<13; kolumn++) {
			printf("%d",jmf[rad][kolumn]);
        }
        printf("\n");
	}
	printf("\n");
    //husnr h�js (1 g�ng/hus)+1 d�r +1 st�r f�r det sista oanv�nda husnr.
	printf("P� fotot finns %d hus",husnr-1);
	fclose(fil);
}
/*
alt l�sning som fungerar endast f�r vertikala ELLER horisontella hus:
    -forloop kollar igenom matrisen, hittas ett x start en while-loop som kollar alla
    4 omkringliggande rutor (N,S,V,�) efter flera x.
    -hittas ett x forts�tter while-loopen leta efter fler x i samma riktning. N�r inga
    fler x hittas slutar s�kningen.
    -markera genoms�kta rutor med punkter.
*/
