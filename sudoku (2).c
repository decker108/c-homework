#include <stdlib.h>
#include <stdio.h>

int testa(int rad,int kol,int tal,int tab[9][9]) {
    int i,j;
    for (i=0;i<9;i++) {
        if (tab[rad][i]==tal) {
            return 0;
        }
    }
    for (i=0;i<9;i++) {
        if (tab[i][kol]==tal) {
            return 0;
        }
    }
    for (i=rad/3*3;i<=rad/3*3+2;i++) {
        for (j=kol/3*3;j<=kol/3*3+2;j++) {
            if (tab[i][j]==tal) {
                return 0;
            }
        }
    }
    return 1;
}

void skrivut(int tab[][9]) {
    int rad,kolumn;
    for (rad=0; rad<9; rad++) {
    	for (kolumn=0; kolumn<9; kolumn++) {
    		printf("%d ",tab[rad][kolumn]);
    	}
    	printf("\n");
    }
}

void solve(int n,int tab[9][9]) {
    int i,j,k;
    if (n==81) {
        skrivut(tab);
    } else {
        for (i=0;i<9;i++) {
            for (j=0;j<9;j++) {
                if (tab[i][j]==0) { //om tab[i][j] �r noll, g� till ut f�r att l�sa
                    goto ut;
                }
            }
        }
    ut:
        for (k=1;k<=9;k++) {
            if (testa(i,j,k,tab)) {
                tab[i][j]=k;
                solve(n+1,tab);
                tab[i][j]=0; //markera att senaste talet ej �r en l�sning
            }
        }
    }
}

int main(void) {
    FILE *fil;
    int i,j,n=0,tab[9][9];
    fil=fopen("sudoku.txt","rt");
    if (fil == NULL) {
        printf("Fil ej funnen");
        exit(-1);
    }
    for (i=0;i<9;i++) {
        for (j=0;j<9;j++) {
            fscanf(fil,"%d",&tab[i][j]);
            if (tab[i][j]!=0) {
                n++;
            }
        }
    }
    fclose(fil);
    solve(n,tab);
}
