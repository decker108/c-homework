//CL080119 Uppgift 8 Speglar
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main(void) {
	//variabler
	char bana[7][8];
	char tecken;
	int riktning; //vilken riktning ljuset f�rdas i (1=upp, 2=ned, 3=h�ger, 4=v�nster)
	const int upp=1,ned=2,hoger=3,vanster=4;
	FILE *spegelfil;
	spegelfil=fopen("080119-8.txt","rt");

	//l�s in filens inneh�ll i en matris
	for (int a=0; a<7; a++) {
		fscanf(spegelfil,"%s",bana[a]);
		//printf("%s \n",bana[a]); //testutskrift f�r att se om banan l�ses in r�tt
	}

	//startposition och riktning.
	int x=0; //koordinat x (0 = l�ngst till h�ger)
	int y=0; //koordinat y (0 = �versta raden)
	riktning=hoger; //riktning h�ger

	//genomf�r ljusets f�rd
	do {
	    //vilket tecken st�r ljuset p�?
	    tecken=bana[y][x]; //x �r rad, y �r kolumn
	    //printf("%c",tecken);
	    if (tecken=='\\') { //backslash F�R INTE vara enkel
	        //printf("back");
	    	switch (riktning) {
                case upp: riktning=vanster; break; //riktning upp + \ = ny riktning v�nster
                case ned: riktning=hoger; break; //riktning ned + \ = ny riktning h�ger
                case hoger: riktning=ned; break; //riktning h�ger + \ = ny riktning ned
                case vanster: riktning=upp; break; //riktning v�nster + \ = ny riktning upp
            }
            //printf("Tar ett steg MED spegling ");
	    } else if (tecken=='/') { //slash F�R vara enkel
	        //printf("fram");
            switch (riktning) {
                case upp: riktning=hoger; break; //riktning upp + / = ny riktning h�ger
                case ned: riktning=vanster; break; //riktning ned + / = ny riktning v�nster
                case hoger: riktning=upp; break; //riktning h�ger + / = ny riktning upp
                case vanster: riktning=ned; break; //riktning v�nster + / = ny riktning ned
            }
            //printf("Tar ett steg MED spegling ");
        } else {
            //Om ljuset varken st�r p� ett / eller ett \ �r riktningen of�r�ndrad.
            //riktning=riktning;
            //printf("Tar ett steg utan spegling... ");
        }
        //printf("(x,y)=(%d,%d)",x,y);
        //ljusets riktning best�mmer vilken koordinat som ska �ndras.
	    switch (riktning) {
	        case upp: y--; break;    //ex (4,5) blir (4,4)
	        case ned: y++; break;    //ex (0,0) blir (0,1)
	        case hoger: x++; break;  //ex (0,0) blir (1,0)
	        case vanster: x--; break;//ex (4,5) blir (3,5)
	    }
	    //printf(" till (x,y)=(%d,%d)\n",x,y);
    }while (y>=0 && y<7 && x>=0 && x<7); //nuvarande koordinater>=gr�ns koordinater
    printf("Str�len l�mnar bordet p� rad %d \n",y+1);
}
/*
L�s in hela "banan" i en matris.
Do-while loop (slutar n�r ljuset hamnar utanf�r rutt-matrisen):
    1. Ljuset f�rdas en ruta per varv i loopen.
    2. F�r varje varv kontrolleras rutan ljuset st�r p�.
    3. N�r en spegel p�tr�ffas kollas vilken riktning ljuset �ker i och vilken
       spegeltypen �r. Dessa tv� faktorer best�mmer vilken v�g ljuset tar efter det.
       N�r en en spegel p�tr�ffats har riktningen bytts ut.

(1=upp, 2=ned, 3=h�ger, 4=v�nster)
Ljuset f�rdas upp�t: / ger ny riktning �t h�ger, \ ger ny riktn �t v�nster
Ljuset f�rdas ned�t: / ger ny riktn �t v�nster, \ ger ny riktn �t h�ger
Ljuset f�rdas �t h�ger: / ger ny riktn upp�t, \ ger ny riktn ned�t
Ljuset f�rdas �t v�nster: / ger ny riktn ned�t, \ ger ny riktn upp�t

Startposition: (0,0) dvs i �versta v�nstra h�rnet
Slut: Ljuset slutar alltid med riktn �t h�ger eller v�nster
*/
