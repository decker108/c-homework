#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

struct objekt{
   int tal;
   struct objekt *left,*right;
};
typedef struct objekt objekt;

objekt *InsertTree(objekt *p,objekt ny){
  objekt *z;

  if(p==NULL){
    z=(objekt *) malloc(sizeof(objekt));
    *z=ny;
    z->left=NULL;
    z->right=NULL;
    return z;
  }
  else
    if(ny.tal<p->tal)
      p->left=InsertTree(p->left,ny);
    else
      p->right=InsertTree(p->right,ny);
    return p;
}

objekt *Init(void){
  objekt p,*start;
  int i,t,n;
  FILE *infil;

  infil=fopen("uppg5.dat","rt");
  fscanf(infil,"%d",&n);
  start=NULL;
  for(i=1;i<=n;i++){
    fscanf(infil,"%d",&t);
    p.tal=t;
    p.right=NULL;
    p.left=NULL;
    start=InsertTree(start,p);
  }
  fclose(infil);
  return start;
}

// H�r ska funkionen PrintTreeLevel placeras

int main(void){
   objekt *start;

   start=Init();
   PrintTreeLevel(start);
   getch();
}
