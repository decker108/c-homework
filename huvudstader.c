//CL071026 Uppg6 Huvudst�der
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main(void) {
	//variabler
	int val=0,x=0,y=0,antal_huvudstader=0,antal_lander;
	char vald_varldsdel[15],test_land[40],test_varldsdel[15],test_huvudstad[25];
	char land[200][40];
	char huvudstad[200][25];

	//fr�ga efter v�rldsdel
    printf("Europa \nSydamerika \nNordamerika \nAsien \nAfrika \nOceanien \n");
    printf("V�lj en v�rldsdel: ");
    scanf("%s",vald_varldsdel);
    printf("\n");

    //fil-l�snings-kod f�r varldsdel.txt
    FILE *filA;
    filA=fopen("varldsdel.txt","rt");
    fscanf(filA,"%d", &antal_lander);

    //l�s igenom fil och se vilka l�nder som finns i v�rldsdelen
    for (int a=1; a<=antal_lander; a++) {
    	fscanf(filA, "%s", test_land);
    	fscanf(filA, "%s", test_varldsdel);
    	if ((strcmp(test_varldsdel,vald_varldsdel))==0) { //finns inl�sta-landet i r�tt v�rldsdel?
            strcpy(land[x],test_land); //mottagare,s�ndare
            x++;
    	}
    }
    fclose(filA);

    //fil-l�snings-kod f�r huvudstad.txt
    FILE *filB;
    filB=fopen("huvudstad.txt","rt");
    fscanf(filB,"%d", &antal_lander);

    //l�s igenom fil och se vilka huvudst�der som finns i ovanst�ende l�nder
    for (int a=1; a<=antal_lander; a++) {
        fscanf(filB, "%s", test_land);
        fscanf(filB, "%s", test_huvudstad);
        for (int c=0; c<=199; c++) {
            if ((strcmp(test_land,land[c]))==0) { //Finns inl�sta landet med i land-arrayen?
                strcpy(huvudstad[y],test_huvudstad); //Om sant, spara huvudstaden i array (mottagare,s�ndare)
                antal_huvudstader++;
                y++;
            }
        }
    }
    fclose(filB);

    //skriv ut alla huvudst�ders namn i v�rldsdelen samt antalet huvudst�der.
    for (int d=0; d<=antal_huvudstader; d++) { //m�ste b�rja p� 0!
    	printf("%s \n",huvudstad[d]);
    }
    printf("Antal huvudst�der: %d \n",antal_huvudstader);
}
/*
tv� textfiler anv�nds: varldsdel.txt och huvudstad.txt
F�rsta raden i filerna anger antal l�nder (tex 197)

varldsdel.txt
1 Land
2 Landets varldsdel

huvudstad.txt
1 Land
2 Huvudstad
*/
