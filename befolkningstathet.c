//CL051021 Uppg 4 Befolkningst�thet
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct land{
    char namn[80];
    int fmangd;
    float areal,tathet;
};

int compare (const void *a, const void *b){
  return (((struct land*)b)->tathet - ((struct land*)a)->tathet); //st�rre minus mindre
}

int main(void) {
    //variabler
    char list_landsnamn[11][80];
    float list_tathet[11]={0};
	struct land listpost[11];
	int maxtathet=0;
	FILE *fil;
	fil=fopen("jorden.dat","rb");

    //huvudloopen
    int i=0;
	while (!feof(fil)) { //forts�tt tills filens slut.
        fread(&listpost[i],sizeof(struct land),1,fil);
        listpost[i].tathet=listpost[i].fmangd/listpost[i].areal; //r�kna folkt�thet.
        if (i<10) { //l�s in 11 poster.
            i++;
        } else { //n�r den sista posten l�sts in, sortera de tio i sjunkande ordning.
            qsort(listpost,11,sizeof(struct land),compare);
        } //endast den elfte platsen kommer att bytas ut tills fil-slut.
	}

	//skriv ut resultat
	for (int a=0; a<10; a++) {
        printf("%2d. %-15s %7.1f\n",a+1,listpost[a].namn,listpost[a].tathet);
	}
	fclose(fil);
}

/*
1. l�s in 10 f�rsta posterna i en tabell
2. sortera tabellen.
3. l�s in n�sta post i elfte positionen
4. sortera tabellen.
5. upprepa 3 och 4 med alla poster
*/
