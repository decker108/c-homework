#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include <string.h>
#include <time.h>
#include <assert.h>

int antal = 0;

struct persontyp{
    char personnummer[13];
    char fornamn[11];
    char efternamn[13];
};
typedef struct persontyp persontyp;
persontyp *personList;

struct nyckeltyp{
    char personnummer[13];
    int postnummer;
};
typedef struct nyckeltyp nyckeltyp;
nyckeltyp *indexList;

// tal: v�rdet som s�ks i arrayen
// a[]: array som genoms�ks
//  n1: arrayens storlek
int binarsokning(char tal[13],nyckeltyp a[],int n1){
    int u = 0, n = n1-1, k;
    do {
        k = (u+n)/2;
        if (strcmp(tal,a[k].personnummer) < 0) {
            n = k-1;
        } else {
            u = k+1;
        }
    } while (strcmp(tal,a[k].personnummer) != 0 && u <= n);
    if (strcmp(tal,a[k].personnummer) == 0) {
        return k;
    } else {
        return n1+1;
    }
}

int indexsokning(char personnummer[13], char fnamn_output[11], char enamn_output[13]) {
    int r = binarsokning(personnummer,indexList,antal);
    if (r <= antal) {
        strcpy(fnamn_output,personList[indexList[r].postnummer].fornamn);
        strcpy(enamn_output,personList[indexList[r].postnummer].efternamn);
        return 1;
    }
    return 0;
}

//s�k igenom filen efter f�r- och efternamn
void linjarsokning(char fornamn[11], char efternamn[13]) {
    FILE *fil;
    fil = fopen("register.dat","rt");
    
    char pnr_input[13];
    char fnamn_input[11];
    char enamn_input[13];
    
    while (!feof(fil)) {
        fscanf(fil,"%12s %10s %12s!",pnr_input,fnamn_input,enamn_input);
        if (strcmp(fnamn_input,fornamn) == 0 && strcmp(enamn_input,efternamn) == 0) {
            break;
        }
    }
    fclose(fil);
}

int order(const void *a, const void *b) {
    return strcmp(((nyckeltyp *)a)->personnummer,((nyckeltyp *)b)->personnummer);
}

void init2() {
    FILE *fil;
    fil = fopen("register.dat","rt");
    assert(fil != NULL);
    
    persontyp *input;
    persontyp *tmpA;
    nyckeltyp *tmpB;
    
    personList = NULL;
    indexList = NULL;
    char pnr_input[13];
    char fnamn_input[11];
    char enamn_input[13];
    
    while (!feof(fil)) {
        fscanf(fil,"%12s %10s %12s!",pnr_input,fnamn_input,enamn_input);
        antal++;
        
        tmpA = (persontyp*) realloc(personList, antal*sizeof(persontyp));
        tmpB = (nyckeltyp*) realloc(indexList, antal*sizeof(nyckeltyp));
        
        if (tmpA != NULL && tmpB != NULL) {
            personList = tmpA;
            strcpy(personList[antal-1].personnummer,pnr_input);
            strcpy(personList[antal-1].fornamn,fnamn_input);
            strcpy(personList[antal-1].efternamn,enamn_input);
            
            indexList = tmpB;
            strcpy(indexList[antal-1].personnummer,pnr_input);
            indexList[antal-1].postnummer = antal-1;
        } else {
            free(personList);
            puts("Error (re)allocating memory");
            exit(1);
        }
    }
    
    fclose(fil);
}

int main(void) {
    srand(time(0));
	init2();

    int i;
    double a = 0, b = 0;
    clock_t start, end;
    double elapsed;
	for (i=0; i<10000; i++) {
		//slumpa personnummer fr�n index
		char slumpnr[12];
        int slumptal = rand()%antal;
        strcpy(slumpnr,indexList[slumptal].personnummer);
        
        //bin�rsokning
        char fnamn_output[10], enamn_output[12];
        start = clock();
        indexsokning(slumpnr,fnamn_output,enamn_output);
        end = clock();
        elapsed = ((double) (end - start)) / CLOCKS_PER_SEC;
        a = a + elapsed;
        
        //linj�rsokning
        start = clock();
        linjarsokning(fnamn_output,enamn_output);
        end = clock();
        elapsed = ((double) (end - start)) / CLOCKS_PER_SEC;
        b = b + elapsed;
	}
	
    double tidA = a / 10000.0;
    double tidB = b / 10000.0;
	printf("Indexs\x94kningen tog %f sek \nBin\x84rs\x94kningen tog %f sek",a,b);
	
	return 0;
}
