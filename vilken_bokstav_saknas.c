//CL050315 Uppg 7 Vilken bokstav saknas?
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char alfabetet[30]={"abcdefghijklmnopqrstuvwxyz���"};

//returnerar 1 om antalord st testord med * ersatt av bokstav finns i lexikonet
int ordsokning (char bokstav, char ord[10][16], int antalord) {
    int i,j;

    //kopiera in alla ord i testord-matrisen
    char testord[10][16];
    for (i=0; i<antalord; i++) {
    	strcpy(testord[i],ord[i]);
    }

    //hitta asterisken och byt ut mot en bokstav p� alla ord i matrisen
    for (j=0; j<antalord; j++) {
        for (i=0; i<strlen(testord[j]); i++) {
            if (testord[j][i]=='*') {
                testord[j][i]=bokstav;
                break;
            }
        }
    }

    //fil-kod
    int antal_lexord;
    FILE *lexikonfil;
    lexikonfil=fopen("saolord2.txt","rt");
    fscanf(lexikonfil,"%d",&antal_lexord);

    //genoms�k lexikonet, hitta matchande ord
    char jmford[16];
    int hittad[10]={0};
    for (i=0; i<antal_lexord; i++) {
        fscanf(lexikonfil,"%s",jmford);
        for (j=0; j<antalord; j++) { //ett varv per testord i matrisen
            if(strcmp(jmford,testord[j])==0) {
                //n�r ett ord hittats, markera i hittad-arrayen
                hittad[j]=1;
            }
        }
    }
    rewind(lexikonfil);
    fclose(lexikonfil);

    int antal_hittade=0;
    for (i=0; i<antalord; i++) {
        if (hittad[i]==1) {
            antal_hittade++;
        }
    }
    if (antal_hittade==antalord) {
        return 1;
    } else {
        return 0;
    }
}

int main(void) {
	//variabler
	char testord[10][16]; //Inneh�ller de inl�sta orden fr�n fleraord.txt
	char bokstav;
	int antalord;
    FILE *inputfil;
    inputfil=fopen("fleraord.txt","rt");
    //inputfil=fopen("testfil.txt","rt");
    fscanf(inputfil,"%d",&antalord);

    int a;
    for (a=0; a<antalord; a++) {
    	fscanf(inputfil,"%s",testord[a]);
    }
    fclose(inputfil);

    int i,hittad=0;
    int antal_bokstaver=strlen(alfabetet);
    for (i=0; i<antal_bokstaver; i++) {
        if (ordsokning(alfabetet[i],testord,antalord)) {
            bokstav=alfabetet[i];
            hittad=1;
            break;
        }
    }
    if (hittad==1) {
        printf("Bokstaven som saknas �r %c (%c)",bokstav,148);
    } else {
        printf("Inga matchande ord\n");
    }
}
/*
1. leta igenom ordet efter *.
2. kopiera ord till tempvariabel.
3. byt ut * mot a,b,c,osv (lagra bokstaven i tempvariabel).
4. leta efter det nya ordet i lexikonfilen.
    4a. om ordet hittas, beh�ll bokstaven och g� vidare till n�sta ord.
    4b. om ordet inte hittas, byt bokstaven och b�rja om med f�rsta ordet.
5. om alla ord hittats, skriv ut bokstaven som anv�ndes.
*/
