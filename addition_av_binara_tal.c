//CL050825 Uppg 4 Addition av bin�ra tal
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main(void) {
	//variabler
	const int x=21;
	char tal_a[x],tal_b[x];
	int bintal_a[x]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; //f�rsta talet
	int bintal_b[x]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; //andra talet
	int bintal_c[x]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; //summan
                    //0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20

	//l�s in talen (som 1 och 0)
	printf("Tal 1? ");
	scanf("%s",tal_a);
	printf("Tal 2? ");
	scanf("%s",tal_b);

    //g�r om str�ng till siffror, l�gg str�ngens f�rsta siffra i arrayens sista ruta.
	for (int i=0; i<strlen(tal_a); i++) { //g� fr�n str�ngens index 0 till maxl�ngden
		int index_a=x-(strlen(tal_a))+i; //arrayens l�ngd-str�ngens l�ngd+index
		bintal_a[index_a]=(int)(tal_a[i]-'0'); //tal hamnar i slutet, fr�n h�ger till v�nster.
		//(int)(tal_a[i]-'0') omvandlar till str�ngen till siffror. tal-'0' ger 0 lr 1
		//printf("%d: %d %s",i,bintal_a[index_a],tal_a); //testutskrift av f�rsta talet
	}
	for (int i=0; i<strlen(tal_b); i++) {
		int index_b=x-(strlen(tal_b))+i;
		bintal_b[index_b]=(int)(tal_b[i]-'0');
		//printf("%d: %d %s",i,bintal_b[index_b],tal_b); //testutskrift av f�rsta talet
	}

	//omvandla talen till bin�rtal
	int minnessiffra=0;
    for (int i=x-1; i>=0; i--) {
        int a=bintal_a[i],b=bintal_b[i];
    	int summa=a+b+minnessiffra;
    	if (summa==1) {
    		bintal_c[i]=1;
    		minnessiffra=0;
    	} else if (summa==0 && minnessiffra==1) {
    	    bintal_c[i]=1;
    	    minnessiffra=0;
    	} else if (summa==2) {
    	    bintal_c[i]=0;
    	    minnessiffra=1;
    	} else if (summa==3) {
    	    bintal_c[i]=1;
    	    minnessiffra=1;
    	}
    }
    for (int c=0; c<x; c++) {
    	printf("%d",bintal_c[c]);
    }
}
/*
0+0 0
0+1 1
1+0 1
1+1 2

*/
