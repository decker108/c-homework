//CL051021 Uppgift 7 - Fyll i kvadraten
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int blocktest(int stormatris[4][4],int testsiffror[4],int x[4],int y[4]) {
    //s�tt in testsiffrorna i stormatrisen
    int i;
    for (i=0; i<4; i++) {
        stormatris[x[i]][y[i]]=testsiffror[i];
    }

//    int radx,kolumnx,a=0;
//    for (radx=0; radx<4; radx++) {
//    	for (kolumnx=0; kolumnx<4; kolumnx++) {
//    	    printf("%d ",stormatris[radx][kolumnx]);
//    	}
//    	printf("\n");
//    }
//    printf("\n");

    //ber�kna summor och j�mf�r
    int summa=stormatris[0][0]+stormatris[0][1]+stormatris[0][2]+stormatris[0][3];
    int nysumma=0,rad=0;
    while (rad<4) {
        nysumma=stormatris[rad][0]+stormatris[rad][1]+stormatris[rad][2]+stormatris[rad][3];
        if (summa!=nysumma) {
            return 1;
        }
        rad++;
    }

    //rad vs kolumn j�mf�relse
    summa=stormatris[0][0]+stormatris[1][0]+stormatris[2][0]+stormatris[3][0];
    if (nysumma!=summa) {
        return 1;
    }

    //ber�kna summor och j�mf�r
    int kolumn=0;
    while (kolumn<4) {
        nysumma=stormatris[0][kolumn]+stormatris[1][kolumn]+stormatris[2][kolumn]+stormatris[3][kolumn];
        if (summa!=nysumma) {
            return 1;
        }
        kolumn++;
    }
    return 0;
}

int main(void) {
    int stortblock[4][4]; //00,01,02,03,10,...
    int litetblock[4]; //0,1,2,3 = �vre h�gra, �vre v�nstra, nedre h�gra, nedre v�nstra
    int x_koord[4],y_koord[4]; //inneh�ller x och y koord f�r nollorna i stortblock
    FILE *fil;
    fil=fopen("matris.txt","rt");

    //l�s in blocket
    int rad,kolumn,a=0;
    for (rad=0; rad<4; rad++) {
    	for (kolumn=0; kolumn<4; kolumn++) {
    		fscanf(fil,"%d",&stortblock[rad][kolumn]);
    		if (stortblock[rad][kolumn]==0){
    		    y_koord[a]=kolumn;
    		    x_koord[a]=rad;
    		    a++;
    		}
    		//printf("%d ",stortblock[rad][kolumn]);
    	}
    	//printf("\n");
    }
    //printf("\n");

    //l�s in ett litet block, testa i stortblock.
        //g� igenom stora blocket, leta efter 0. Hittas 0, s�tt in tal fr�n litet block.
    int matris_nr=1,hittad=0;
    while(!feof(fil)) {
        fscanf(fil,"%d %d %d %d",&litetblock[0],&litetblock[1],&litetblock[2],&litetblock[3]);
        if (blocktest(stortblock,litetblock,x_koord,y_koord)==0) {
            printf("Matris %d ger samma summa",matris_nr);
            hittad=1;
            break;
        } else {
            hittad=0;
        }
        matris_nr++;
    }

    if (hittad==0) {
        printf("Passande matris ej funnen");
    }
    fclose(fil);
}
