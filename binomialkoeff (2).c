#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int antal = 0;

int binomkoeff(int n, int m) {
    antal++;
    if (n == m) {
        return 1;
    } else if (m == 0) {
        return 1;
    } else if (m == 1) {
        return n;
    } else {
        return binomkoeff(n-1,m-1)+binomkoeff(n-1,m);
    }
}

int main(void) {
    int a = 13;
    int b = 4;
    printf("Binomialkoefficienten av %d \x94ver %d = %d \n",a,b,binomkoeff(a,b));
    printf("Antal anrop: %d \n",antal);
	return 0;
}
