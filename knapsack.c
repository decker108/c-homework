#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

#define maxSack 20
#define numTypes 7

int size[numTypes] = {3, 2, 3, 6, 2, 2, 1};
int cost[numTypes] = {180, 235, 500, 1000, 436, 480, 250};

int maxKnapsack(int k){
    int knapsack[maxSack],max,i,type,total;

    knapsack[0] = 0;
    for(i=1;i<=k;i++) {
        max = -INT_MAX;
        for(type=0;type<numTypes;type++) 
            if(i-size[type]>=0) {
                total = knapsack[i-size[type]]+cost[type];
                if(max<total)
                    max = total;
            }
        printf("%d %d\n",i,max);
        knapsack[i] = max;
    }
    return knapsack[k];
}

int main(void) {
//	int noOfElems, totVol;
//    FILE *fil;
//    fil = fopen("knapsack.txt","rt");
//    fscanf(fil,"%d",&totVol);
//    fscanf(fil,"%d",&noOfElems);
//    int *size, *cost, j;
//    size = (int*) malloc(noOfElems*sizeof(int));
//    cost = (int*) malloc(noOfElems*sizeof(int));
//    for (j=0; j<noOfElems; j++) {
//    	fscanf(fil,"%d %d",&size[j],&cost[j]);
//    }
    
    printf("SVAR: %d",maxKnapsack(20));
	return 0;
}
