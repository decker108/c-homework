//CL080119 Uppg 4 IT-museum
#include <stdio.h>
#include <stdlib.h>
int main(void) {
    //variabler
	int antal_besokare,entre_h,entre_m,exit_h,exit_m;
	int max_besokare=0,max_h=0,max_m=0;
	char kolon; //denna variabel ska ej anv�ndas

	//deklarera och �ppna fil
    FILE *fil;
    fil=fopen("itmuseum.txt","rt");
    fscanf(fil,"%d", &antal_besokare);

    //nollst�ll arrayen
    int tidpunkt[24][60]={0};

    //huvudloop (g�r att g�ra finare med: fscanf("%2d %1c %d",&entre_h,&kolon,&entre_m)
    for (int a=1; a<=antal_besokare; a++) { //varje varv g�r igenom 2 rader, max=antal_besokare
    	fscanf(fil, "%2d", &entre_h); //l�s in timmen personen gick in (2 siffror)
    	fscanf(fil, "%1c", &kolon);   //l�s in kolon, detta endast f�r att g� vidare
    	fscanf(fil, "%d", &entre_m);  //l�s in minuten personen gick in (till radslut)
    	fscanf(fil, "%2d", &exit_h);  //l�s in timmen personen gick ut (2 siffror)
    	fscanf(fil, "%1c", &kolon);   //l�s in kolon, detta endast f�r att g� vidare
    	fscanf(fil, "%d", &exit_m);   //l�s in minuten personen gick ut (till radslut)
    	//printf("%d:%d \n%d:%d \n \n",entre_h,entre_m,exit_h,exit_m);

    	int h,m;
    	h=entre_h; //h �r nuvarande timme
    	m=entre_m; //m �r nuvarande minut
    	while (1) { //loopen fungerar som en klocka. den startar n�r bes�karen g� in och stannar n�r bes�karen g�r ut.
            tidpunkt[h][m]++; //varje timmes minut r�knas in, med b�rjan i entre-tiden.
            if (h==exit_h && m==exit_m) { //n�r loopen n�tt exit-tiden bryts den.
                break; //tidpunkts �kning ligger f�re break s� att den sista minuten r�knas.
            }
            m++; //�ka nuvarande minut varje varv
            if (m>=60) { //n�r min n�tt 60, �kas nuvarande h med 1 och min nollst�lls
                m=0;
                h++;
            }
    	}
    	//Denna dubbelloop sparar maxpunkten f�r bes�karantalet och maxpunktens tid
    	for (int h=8; h<=21; h++) { //yttre loopen g�r igenom �ppettidens timmar
    		for (int m=0; m<=59; m++) { //inre loopen g�r igenom dess minuter
    			if (tidpunkt[h][m]>max_besokare) {
                    max_besokare=tidpunkt[h][m];
                    max_h=h;
                    max_m=m;
    			}
    		}
    	}
    }
    printf("St�rsta bes�kstrycket var %d:%d med %d bes�kare. \n",max_h,max_m,max_besokare);
    fclose(fil);
}
/*
g�r en array f�r museets totala �ppettider 08:00-22:00, dvs tidpunkt[25][60]
kolla vilka tider en person var p� museet, �ka arrayens v�rde med +1 f�r varje minut.
n�r alla bes�kare i listan har r�knats, g� igenom arrayen och kolla vilken tidpunkt som har h�gst v�rde.

tidspunkts�kningsvillkor:
om entre_timmen och entre_minuten �r lika med/st�rre �n nuvarande minut OCH exit_timme och exit_minut �r mindre �n nuvarande minut:
�ka tidpunkt[nuvarande timme][nuvarande minut]++
*/
