//F1 u1.6 Eulers funktion
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int gcd(int a, int b) {
    if (b == 0) {
        return a;
    } else {
        return gcd(b,a%b);
    }
}

int euler(int n) {
    int i, antal = 0;
    for (i=1; i<=n; i++) {
    	if (gcd(i,n) == 1) {
    	    antal++;
    	}
    }
    return antal;
}

int main(int argc, char* args[]) {
//    int rad,kol;
//    for (rad=0; rad<10; rad++) {
//    	for (kol=0; kol<10; kol++) {
//    		printf("gcd(%d,%d)=%d \n",rad,kol,gcd(rad,kol));
//    	}
//    }
    int n;
    printf("Skriv in ett tal n: ");
    scanf("%d",&n);
    printf("Antal: %d",euler(n));
}
