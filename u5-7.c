//============================================================================
// Name        : u5-7.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

//#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//using namespace std;

int leafCount = 0;
int nodeCount = 0;

struct objekt {
	char nyckel[10];
	struct objekt *left, *right;
};
typedef struct objekt objekt;

// Rekusiv ins�ttning
objekt *InsertTree(objekt *p, objekt ny) {
	objekt *z;

	if (p == NULL) {
		z = (objekt *) malloc(sizeof(objekt));
		*z = ny;
		z->left = NULL;
		z->right = NULL;
		return z;
	} else {
		if (strcmp(ny.nyckel, p->nyckel) < 0) {
			p->left = InsertTree(p->left, ny);
		} else {
			p->right = InsertTree(p->right, ny);
		}
	}
	return p;
}

// Iterativ ins�ttning
objekt *InsertTree2(objekt *start, objekt ny) {
	objekt *x, *y, *z;

	z = (objekt *) malloc(sizeof(objekt));
	*z = ny;
	y = NULL;
	x = start;
	while (x != NULL) {
		y = x;
		if (strcmp(z->nyckel, x->nyckel) < 0) {
			x = x->left;
		} else {
			x = x->right;
		}
	}
	if (y == NULL) {
		start = z;
	} else {
		if (strcmp(z->nyckel, y->nyckel) < 0) {
			y->left = z;
		} else {
			y->right = z;
		}
	}
	return start;
}

// Borttagning av hela tr�det
void DeleteTree(objekt **x) {
	objekt *z;

	if (*x != NULL) {
		if ((*x)->right == NULL) {
			z = *x;
			*x = (*x)->left;
			free(z);
		} else if ((*x)->left == NULL) {
			z = *x;
			*x = (*x)->right;
			free(z);
		} else {
			z = (*x)->right;
			while (z->left != NULL) {
				z = z->left;
			}
			z->left = (*x)->left;
			z = *x;
			*x = (*x)->right;
			free(z);
		}
	}
}

// S�k given nyckel
objekt *SearchTree(objekt *x, char namn[]) {
	if (x == NULL) {
		return NULL;
	}
	if (x != NULL && strcmp(x->nyckel, namn) == 0) {
		return x;
	}
	if (x != NULL) {
		if (strcmp(namn, x->nyckel) < 0) {
			x = SearchTree(x->left, namn);
		} else {
			x = SearchTree(x->right, namn);
		}
	}
	return x;
}

// S�k fader (y) till given nyckel (x)
objekt *SearchFather(objekt *x, objekt *y, char namn[]) {
	if (x == NULL) {
		return NULL;
	}
	if (x != NULL && strcmp(x->nyckel, namn) == 0) {
		return y;
	}
	if (x != NULL) {
		if (strcmp(namn, x->nyckel) < 0) {
			y = SearchFather(x->left, x, namn);
		} else {
			y = SearchFather(x->right, x, namn);
		}
	}
	return y;
}

// Borttagning av given nod. H�r uppdelat i tv� steg. Jfr boken
void DeleteName(objekt **x, char namn[]) {
	objekt *tmp;

	tmp = SearchFather(*x, NULL, namn);
	if (tmp == NULL) {
		DeleteTree(x);
	} else if (strcmp(tmp->nyckel, namn) == 0) {
		DeleteTree(&tmp->left);
	} else {
		DeleteTree(&tmp->right);
	}
}

// Skriv ut tr�det
void PrintTree(objekt *x) {
	if (x != NULL) {
		PrintTree(x->left);
		printf("%s ", x->nyckel);
		PrintTree(x->right);
	}
}

void NumberOfLeaves(objekt *x) {
	//DFS f�r att hitta noder d�r b�de right och left �r null
	if (x != NULL && (x->left == NULL && x->right == NULL)) {
		leafCount++;
	}
	if (x != NULL) {
		NumberOfLeaves(x->left);
		NumberOfLeaves(x->right);
	}
}

void NumberOfNodes(objekt *current, objekt *parent) {
	//DFS f�r att hitta noder d�r tidigare noden ej �r NULL
	if (parent != NULL) {
		nodeCount++;
	}
	while (current != NULL) {
		NumberOfNodes(current->left,current);
		NumberOfNodes(current->right,current);
	}
}

// N�gra anv�ndningar av funktionerna
int main(void) {
	objekt *start, ny;
	int k;
	FILE *fil;

	start = NULL;
	fil = fopen("bintrad3.dat", "rt");
	for (k = 1; k <= 14; k++) {
		fscanf(fil, "%s", &ny.nyckel);
		ny.left = NULL;
		ny.right = NULL;
		start = InsertTree2(start, ny);
	}
	
//	if (SearchTree(start, "Gustav")) {
//		printf("Finns\n");
//	} else {
//		printf("Finns Ej\n");
//	}
//
//	PrintTree(start);
//	printf("\n");
//	DeleteName(&start, "Jan");
//	PrintTree(start);
//	printf("\n");
//	strcpy(ny.nyckel, "Jan");
//	InsertTree(start, ny);
	
	NumberOfLeaves(start);
	printf("Antal l�v: %d \n",leafCount);
	
	//NumberOfNodes(start,NULL);
	//printf("Antal noder: %d \n",nodeCount);
	
	PrintTree(start);
	printf("\n");

	while (start != NULL) {
		DeleteTree(&start);
	}
	return 0;
}
