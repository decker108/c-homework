#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>

int antal = 0;
char facit[6] = {"ABCDE"};

int robot(char str[6]) {
    if (strcmp(str,facit) == 0) {
        return antal;
    } else {
        antal++;
        if (str[2] - str[0] == 1) { //drag b
            char tmp = str[0];
            str[0] = str[1];
            str[1] = tmp;
            robot(str);
        } else { //drag s
            char tmp2 = str[4];
            int i;
            for (i=4; i>0; i--) {
                str[i] = str[i-1];
            }
            str[0] = tmp2;
        }
        //printf("%s\n",str);
        robot(str);
    }
}

int main(void) {
    char input[6];
//	printf("Hur ligger paketen? ");
//	scanf("%s",input);

    //Inl�sning fr�n fil med alla permutationer av ABCDE
    FILE *fil;
    fil=fopen("abcde.txt","rt");
    char temp[6];
    fscanf(fil,"%s", input);
    while (input != NULL) {
        strcpy(temp,input);
        robot(input);
        printf("Det kr\x84vs %2d steg f\x94r %s \n",antal,temp);
        fscanf(fil,"%s", input);
        antal = 0;
    }
    fclose(fil);

	return 0;
}