//CL040413 Uppg 3 Fyrar
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int hav[19][19]={0};

void ritaljus(int input_rad, int input_kolumn, int input_storlek){
    int rad_upp=input_rad;
    int rad_ner=input_rad;
    int kolumn_h=input_kolumn;
    int kolumn_v=input_kolumn;

    //rita ljuslinje upp�t
    while (rad_upp!=0 || (rad_upp-1)>0) {
        hav[rad_upp-1][input_kolumn]=1;
        rad_upp--;
    }

    //rita ljuslinje ned�t
    while (rad_ner!=input_storlek || (rad_ner+1)<input_storlek) {
        hav[rad_ner+1][input_kolumn]=1;
        rad_ner++;
    }
    //rita ljuslinje �t h�ger
    while (kolumn_h!=input_storlek || (kolumn_h+1)<input_storlek) {
        hav[input_rad][kolumn_h+1]=1;
        kolumn_h++;
    }

    //rita ljuslinje �t v�nster
    while (kolumn_v!=0 || (kolumn_v-1)>0) {
        hav[input_rad][kolumn_v-1]=1;
        kolumn_v--;
    }
}

int main(void) {
	int y_koord,x_koord;
	int storlek,antal_fyrar;

	printf("Havets storlek: ");
	scanf("%d",&storlek);
	printf("Antal fyrar: ");
	scanf("%d",&antal_fyrar);

	//l�s in och best�m fyrarnas positioner
	for (int a=1; a<=antal_fyrar; a++) {
		printf("Rad och kolumn f�r fyr %d: ",a);
		scanf("%d %d",&y_koord,&x_koord);
		hav[y_koord-1][x_koord-1]=2;
	}

	//l�s igenom hav-array, leta efter 2'or (fyrar) och rita 1'or (ljus) i hela deras
	//kolumner och rader.
	for (int rad=0; rad<storlek; rad++) {
		for (int kolumn=0; kolumn<storlek; kolumn++) {
            //om en fyr hittas, rita ljuslinjer i de fyra riktningarna runt fyren
            if(hav[rad][kolumn]==2) {
                ritaljus(rad,kolumn,storlek);
            }
		}
	}

	//skriv ut havet
	for (int b=0; b<storlek; b++) {
		for (int c=0; c<storlek; c++) {
			printf("%d",hav[b][c]);
		}
        printf("\n");
	}

	//L�s igenom hav-array och r�kna antal nollor.
	int antal_tomrutor=0;
	for (int rad=0; rad<storlek; rad++) {
		for (int kolumn=0; kolumn<storlek; kolumn++) {
			if (hav[rad][kolumn]==0) {
                antal_tomrutor++;
			}
		}
	}

	//Skriv ut antalet rutor som ej kan se ngn fyr
	printf("Fr�n %d rutor kan man inte se n�gon fyr",antal_tomrutor);
}
