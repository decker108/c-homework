#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE 10

int graph[SIZE][SIZE];
int antal = 0;

int getExits(int currentPos, int *exits) {
    int var,i=0;
    for (var = 0; var < SIZE; ++var) {
		if (graph[currentPos][var] == 1) {
			exits[i] = var;
			i++;
		}
	}
    return i;
}

void solve(int targetPos) {
	int var;
	for (var = 0; var < 10000; ++var) { //default 10000
		int currentPos = 0;
		while (currentPos != targetPos) {
			antal++;
			//ber�kna antalet utg�ngar
			int exits[SIZE] = {0};
			int noOfExits = getExits(currentPos,exits);
			//slumpa fram n�sta f�rflyttnings riktning
			int nextPos = rand()%noOfExits;
			currentPos = exits[nextPos];
		}
	}
}

int main(void) {
	srand(time(0));
	int n;
	FILE *fil;
	fil = fopen("hus.txt","rt");
	if (fil==NULL) {
	    printf("Fel: Fil ej funnen!");
	    exit(-1);
	}
	fscanf(fil,"%d",&n);
	
	int var,var2;
	for (var = 0; var < SIZE; ++var) {
		for (var2 = 0; var2 < SIZE; ++var2) {
			graph[var][var2] = 0;
		}
	}
	
	int node,path;
	for (var = 0; var < n; ++var) { //l�s in filen till graf
		fscanf(fil,"%d %d",&node,&path);
		graph[node-1][path-1] = 1;
	}
	
//	for (var = 0; var < SIZE; ++var) {
//		for (var2 = 0; var2 < SIZE; ++var2) {
//			printf("%d ",graph[var][var2]);
//		}
//		printf("\n");
//	}
	
	//anv�ndarinput
	int target;
	printf("Till vilket rum: ");
	scanf("%d",&target);
	
	solve(target-1);
	
	printf("I medeltal beh�vs %.2f f�rflyttningar",(float)antal/10000.0);
	
	return 0;
}
