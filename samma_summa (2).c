#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

int part(int n, int m) {
    if ((n == 1 && m >= 0) || (m == 0) || (m == 1)) {
        return 1;
    } else if (m < 0) {
        return 0;
    } else {
        return part(n-1,m)+part(n,m-n);
    }
}

int main(void) {
    int n = 0, m = 0;
	printf("Ange n,m f\x94r part(n,m): ");
	scanf("%d,%d",&n,&m);
	printf("Svar=%d",part(n,m));
	return 0;
}