//CL050315 Uppg 2 St�rsta frekvensen
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char bokstaver[27]={"abcdefghijklmnopqrstuvwxyz"};
int bokstavs_nr[27]={0};

void jmf (char ett_tecken) {
    for (int a=0; a<27; a++) {
        if (ett_tecken==bokstaver[a]) {
            bokstavs_nr[a]++;
        }
    }
}

int main(void) {
    char input[8][51];
    char tecken;
    char max_bokstav;
    int max_antal=0;

    FILE *fil;
    fil=fopen("bokstaver.txt","rt");

    for (int a=0; a<8; a++) {
        fscanf(fil,"%s",input[a]);
    }

    for (int rad=0; rad<8; rad++) {
    	for (int kolumn=0; kolumn<50; kolumn++) {
    		tecken=input[rad][kolumn];
    		jmf(tecken);
    	}
    }

    for (int a=0; a<27; a++) {
    	if (bokstavs_nr[a]>max_antal) {
            max_antal=bokstavs_nr[a];
            max_bokstav=bokstaver[a];
    	}
    }

    printf("Bokstaven %c �r vanligast med %d f�rekomster",max_bokstav,max_antal);
    fclose(fil);
}
