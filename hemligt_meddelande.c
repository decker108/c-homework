//CL071026 Uppg 4 Hemligt meddelande
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main(void) {
	//variabler
	int antal_rader,antal_tecken,jmf,x=0;
	char text[5][9];
	char mall[5][9];
	char klartext[20];

	//deklarera och �ppna fil
    FILE *fil;
    fil=fopen("meddelande.txt","rt");
    fscanf(fil,"%d", &antal_rader);
    fscanf(fil,"%d", &antal_tecken);

    //l�s in texten
    for (int a=0; a<=(antal_rader-1); a++) {
        fscanf(fil, "%s", text[a]);
        //printf("%s \n",text[a]);
    }
    //l�s in mallen
    for (int b=0; b<=(antal_rader-1); b++) {
    	fscanf(fil, "%s", mall[b]);
    	//printf("%s \n",mall[b]);
    }
    //skriv ut tecknen som ligger p� samma matris-koordinater som *-tecknen.
    for (int c=0; c<=4; c++) { //stega igenom mallens rader.
        for (int d=0; d<=8; d++) { //stega igenom mallens tecken.
            if (mall[c][d]=='*') { //om tecknet �r en asterisk...
                klartext[x]=text[c][d]; //...spara motsv bokstav i klartext-str�ngen.
                x++; //g� till n�sta plats i klartext-str�ngen efter skrivning.
            }
        }
    }
    //l�gg till \0 i slutet av str�ngen.
    klartext[x+1]='\0';

    //skriv ut hemligt meddelande och st�ng filen.
    printf("%s",klartext);
    fclose(fil);
}
/*
L�s in varje rad till sin egen array-rad

F�r att hitta r�tt platser i variabeln kan en loop som letar efter asterisker anv�ndas.
Loopen sparar numren p� mallens platser d�r * hittas, numren anv�nds f�r att skriva
ut den hemliga texten.
*/
