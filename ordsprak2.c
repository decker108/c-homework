//CL050602 Uppgift 8 Ordspr�k
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct ordsprak{
    char text[200];
};

int main(void) {
	//variabler
	struct ordsprak mening;
	char input_string[20],test_string[20]={' '},mellanslag[2]={' '}, *tmpStr;
	FILE *fil;

    //fr�ga efter s�kstr�ng och best�m dess l�ngd
	printf("Vilket ord? ");
	scanf("%s",input_string);

    //l�gg till mellanslag f�re och efter ordet
    strcpy(test_string,mellanslag); //g�r s� att test_string endast best�r av ett mellanslag
    strcat(test_string,input_string); //l�gg till input_string i slutet av test_string
    int str_langd=strlen(test_string); //kolla l�ngden p� test_string
    strncat(test_string,mellanslag,str_langd); //anv�nd l�ngden f�r att l�gga till mellanslag i slutet av test_string
    printf("_%s_\n",test_string); //teststr�ng som visar att test-str�ngen har mellanslag
	fil=fopen("ordsprak.dat","rb");
	while (1) {
//    for (int a=0; a<=10; a++) {
        int n = fread(&mening,sizeof(struct ordsprak),1,fil);
        if (n!=1) {
            break;
        }
        //anv�nd strstr med �ndrad input_string
        tmpStr=strstr(mening.text,test_string); //kolla efter den h�gra i den v�nstra
        if (tmpStr!=NULL) {
            printf("%s\n",mening.text);
        }
	}
	fclose(fil);
}
/*
metastr�ng-funk: strstr(str1,str2);

1. L�s in s�k-str�ng
2. l�s igenom bin�rfilen och j�mf�r med s�kstr�ng.
   2a. Kolla str�ngl�ngd f�r s�kstr�ng, j�mf�r med det som hittas i s�kningen.
   2b. Om l�ngderna st�mmer, skriv ut.
*/
