#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

struct data {
    double v;
    double w;
};
typedef struct data Data;

void knapsack(Data *data, double c, double f[], int n) {
	int i;
	for (i=0; i<n; i++) {
		f[i] = 0;
	}
	int rem = c;
	i = 0;
	int fits;
	if (data[0].w <= c) {
	    fits = 1;
	} else {
	    fits = 0;
	}
	while (fits && i <= n) {
	    f[i] = 1.0;
	    rem = rem - data[i].w;
	    i++;
	    if (data[i].w <= rem) {
	        fits = 1;
	    } else {
	        fits = 0;
	    }
	}
	if (i<=n) {
	    f[i] = rem / data[i].w;
	}
}

int jmf(const void *ra, const void *rb) {
    Data *a = (Data*)ra;
    Data *b = (Data*)rb;
    if (a->v/a->w > b->v/b->w) {
        return -1; //org 1
    } else if (a->v/a->w == b->v/b->w) {
        return 0;
    } else {
        return 1; //org -1
    }
}

int main(void) {
	int n;
	double c;
    FILE *fil;
    fil = fopen("knapsack.txt","rt");
    fscanf(fil,"%lf",&c); //printf("c=%f \n",c);
    fscanf(fil,"%d",&n); //printf("n=%d \n",n);
    //double *w, *v; 
    Data *data;
    int j;
    double *f;
//    w = (double*) malloc(n*sizeof(double)); //vikt wi
//    v = (double*) malloc(n*sizeof(double)); //volym vi
    data = (Data*) malloc(n*sizeof(Data));
    f = (double*) malloc(n*sizeof(double)); //resultat
    for (j=0; j<n; j++) {
    	fscanf(fil,"%lf %lf",&data[j].v,&data[j].w);
//    	printf("v=%f w=%f \n",data[j].v,data[j].w);
    }
//    printf("\n");
    
    qsort(data,n,sizeof(Data),&jmf);
    int x;
    for (x=0; x<n; x++) {
    	printf("v=%f w=%f \n",data[x].v,data[x].w);
    }
    
    knapsack(data,c,f,n);
    int k;
    for (k=0; k<n; k++) {
    	printf("f[%d]=%f \n",k,f[k]);
    }
	return 0;
}

