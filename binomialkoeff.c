#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int antal = 0;

int fact(int n) { //fakultet med rekursion
    antal++;
    if (n==0) {
        return 1;
    } else {
        return n*fact(n-1);
    }
}

int bin(int n, int m) {
    //antal++;
    if (n < m) {
        return 0;
    } else if (n == m || m == 0) {
        return 1;
    } else if (m == 1) {
        return n;
    } else {
        return (fact(n)/(fact(m)*fact(n-m)));
    }
}

int main(void) {
    printf("%d",bin(13,4));
    printf("\nAntal anrop: %d",antal);
	return 0;
}
