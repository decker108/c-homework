//CL050602 Uppg 6 Lustiga Huset
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main(void) {
	//variabler
	int omkrets=0;
	int x_langd=0,y_langd=0,antal_rader,antal_tecken;
	char rad[11][12];
	char x1,x2,x3;
	char punkt='.',asterisk='*';
	FILE *fil;

	//l�s in antal rader och antal tecken
	fil=fopen("lustigahuset.txt","rt");
	//fil=fopen("050602-6.txt","rt");
	fscanf(fil,"%d",&antal_rader);  //textfilens l�ngd
	fscanf(fil,"%d",&antal_tecken); //textfilens bredd

    //l�s in filen i matris
	for (int a=0; a<antal_rader; a++) {
		fscanf(fil,"%s",rad[a]);
		printf("%s\n",rad[a]);
	}

    //g� igenom matrisen och r�kna ut l�ngden i nord-sydlig riktning.
        //kolla inl�st tecken och tecken till h�ger och v�nster, j�mf�r str�ngen med de tre fallen.
        //om n�got av m�nstren hittas, �ka omkrets med +2m.
    for (int a=0; a<antal_rader; a++) {
        for (int b=0; b<=antal_tecken-2; b++) {
            x1=rad[a][b];
            x2=rad[a][b+1];
            x3=rad[a][b+2];
            if (x1=='.' && x2=='#' && x3=='.') {
                y_langd+=2; //r�knas som tv� v�ggar
            } else if (x1=='.' && x2=='#' && x3=='#') {
                y_langd+=1; //r�knas som en v�gg
            } else if (x1=='#' && x2=='#' && x3=='.') {
                y_langd+=1; //r�knas som en v�gg
            } else {
                //hittas inte de tre ovanst�ende m�nstren h�nder inget...
            }
        }
    }
    //g� igenom matrisen och r�kna ut l�ngden i �st-v�stlig riktning.
    for (int a=0; a<antal_rader-2; a++) {
    	for (int b=0; b<=antal_tecken; b++) {
    		x1=rad[a][b];
    		x2=rad[a+1][b];
    		x3=rad[a+2][b];
    		if (x1=='.' && x2=='#' && x3=='.') {
    			x_langd+=2;
    		} else if (x1=='.' && x2=='#' && x3=='#') {
    		    x_langd+=1;
    		} else if (x1=='#' && x2=='#' && x3=='.') {
    		    x_langd+=1;
    		} else {
                //hittas inte de tre ovanst�ende m�nstren h�nder inget...
    		}
    	}
    }
    omkrets=x_langd+y_langd;
	//skriv ut resultat
	printf("\nL�ngd i y-led: %d\n",y_langd);
	printf("L�ngd i x-led: %d\n",x_langd);
	printf("Husets omkrets �r: %d",omkrets*2);
	fclose(fil);
}
/*
F�r att ber�kna nord-syd l�ngd, titta efter .#. (r�knas som 2), .## (r�knas som 1), ##. (r�knas som 1).
F�r att ber�kna �st-v�st l�ngd, titta efter:
 . | . | #  V�nstra r�knas som 2m.
 # | # | #  Mitten r�knas som 1m.
 . | # | .  H�gra r�knas som 1m.

 strstr(rad[a][b],".*."); kanske kan anv�ndas?
 //r�tta svar ska vara y_langd=30 och x_langd=26 och omkrets=52*2=112
*/
