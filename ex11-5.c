#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

#define INF 999

int nodes[4][4] = {{0,10,INF,7},
                   {10,0,16,2},
                   {INF,16,0,19},
                   {7,2,19,0}};
int res[4][4];

int min(int a, int b) {
    if (a<b) {
        return a;
    } else {
        return b;
    }
}

void floyd(int n) {
    int i,j,k;
    for (i=0; i<n; i++) {
    	for (j=0; j<n; j++) {
    		for (k=0; k<n; k++) {
    			res[i][j] = min(nodes[i][j],nodes[i][k]+nodes[k][j]);
    		}
    	}
    }
}

int main(void) {
	floyd(4);
	int a,b;
	for (a=0; a<4; a++) {
		for (b=0; b<4; b++) {
			printf("%2d ",res[a][b]);
		}
		printf("\n");
	}
	return 0;
}
