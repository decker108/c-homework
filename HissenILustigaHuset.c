#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

int min_rides = INT_MAX;
int dest = 0, up = 0, down = 0, total_levels = 0;

void hissen(int level, int rides) {
    if (level == dest) {
        if (rides < min_rides) {
            min_rides = rides;
        }
    } else {
        if (level <= dest && level+up <= total_levels) {
            hissen(level+up,rides+1);
        }
        if (level >= dest && level-down >= 0) { //
            hissen(level-down,rides+1);
        }
    }
}

int main() {
    while (1) {
        printf("Hur m\x86nga v\x86ningar har huset (1-100): ");
        scanf("%d",&total_levels);
        if (total_levels >= 1 && total_levels <= 100) {
            break;
        }
    }
	printf("F\x94rflyttning upp\x86t: ");
	scanf("%d",&up);
	printf("F\x94rflyttning ned\x86t: ");
	scanf("%d",&down);
	printf("Till vilken v\x86ning ska du: ");
	scanf("%d",&dest);
	
	hissen(1,0); //1p, 0 coins
	if (min_rides != 0) {
        printf("Det beh\x94vs minst %d resor f\x94r att n\x86 m\x86let",min_rides);
	} else {
	    printf("M\x86let kan ej n\x86s!");
	}
	return 0;
}