#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

int f(float n) {
    if (n == 1.0) {
        return 0;
    } else {
        return f(ceil(n/2)) + f(floor(n/2)) + n - 1;
    }
}

int main(void) {
    int n = 0;
	printf("Hur m\x86nga tal ska sorteras? ");
	scanf("%d",&n);
	printf("F\x94r detta kr\x84vs %d j\x84mf\x94relser",f(n));
	return 0;
}
/*
f(16) = 49 (egentligen 64)
f(1000) = 8097!? (egentligen 8977)
Varf�r inte: T(n) = 2T(n/2) + n
*/