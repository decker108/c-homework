#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int resultat = 0;

void siffersumma(int n){
    if(n<10) {
        //printf("%d \n",n);
        resultat += n;
    } else {
        siffersumma(n/10);
        //printf("%d ",n%10); //denna rad n�s endast i slutet
        resultat += n%10;
    }
}

int main(void) {
    int n;
    printf("Skriv in ett tal n: ");
    scanf("%d",&n);
    siffersumma(n);
	printf("Siffersumman av %d �r %d",n,resultat);
	return 0;
}
