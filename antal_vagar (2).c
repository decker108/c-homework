//F1 Uppg 1.5
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int antal = 0;
int v = 0;

int alg(int m, int n) {
    antal++;

    if (m == 0 && n == 0) {
        v++;
    } else {
        if (m>0) {
            printf("v");
            alg(m-1,n);
        }
        if (n>0) {
            printf("h");
            alg(m,n-1);
        }
        printf("u");
    }
}

int main(int argc, char* args[]) {
//    int rad,kol;
//	for (rad=0; rad<10; rad++) {
//		for (kol=0; kol<10; kol++) {
//			printf("Antal vagar(%d,%d): %6d  (antal anrop=%d)\n",rad,kol,alg(rad,kol),antal);
//			antal = 0;
//		}
//	}
    alg(2,3);
	exit(EXIT_SUCCESS);
}
