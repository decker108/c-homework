#include <stdio.h>
#include <conio.h>
int count;

void forsok(int row, int ld, int rd){
    if (row == 0xFF) {
        count++;
    } else {
        int poss = 0xFF & ~(row | ld | rd);
        while (poss){
            int p = poss& -poss;
            poss = poss -p;
            forsok(row+p, (ld+p)<<1, (rd+p)>>1);
        }
    }
}

int main(int argc, char *argv[]){
    printf("Eight Queens\n");
    count = 0;
    forsok(0,0,0);
    printf("Number of solutions is %d\n", count);
}
