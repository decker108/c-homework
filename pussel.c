#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

#define EMPTY 0

int min = INT_MAX;

int test (int arr[3][3]) {
    int jmfarr[3][3] = {{-1,0,-1},{1,2,3},{4,5,6}};
    int rad,kol;
    for (rad = 0; rad < 3; rad++) {
    	for (kol = 0; kol < 3;  kol++) {
    		if (arr[rad][kol] != jmfarr[rad][kol]) {
    		    return 0;
    		}
    	}
    }
    return 1;
}

void funk(arr[3][3],int drag) {
    int tmp; //flyttad pusselbit
	if (test(arr)) {
	    if (drag < min) {
	        min = drag;
	    }
	} else {
        if (arr[0][1] == EMPTY && arr[1][1] != EMPTY) {
            tmp = arr[0][1];
            arr[0][1] = arr[1][1];
            arr[1][1] = tmp;
            funk(arr,drag+1);
            arr[1][1] = arr[0][1];
            arr[0][1] = tmp;
        }
        //andra möjliga förflyttningar
	}
	return;
}

int main(void) {
    int arr[3][3] = {{-1,0,-1},{0,0,0},{0,0,0}};
	FILE *fil;
	fil = fopen("pussel.txt","rt");
	int i,j,rad=0;
	for (i=0; i<6; i++) {
	    if (i==3) {
	        rad++;
	    }
		fscanf("%d",&j);
		arr[rad][i] = j;
	}
	pussel(arr,0);
	return 0;
}