#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int testa_blocket(int matris[3][3]) {
    int siffror[10]={0}; //siffrorna 0-9, 0 anv�nds ej

    for (int rad=0; rad<3; rad++) {
    	for (int kolumn=0; kolumn<3; kolumn++) {
    		siffror[matris[rad][kolumn]]=1;
    	}
    }
    for (int i=1; i<10; i++) {
    	if (siffror[i]==0) {
            return 0;
    	}
    }
    return 1;
}

int main(void) {
	int block[9][9]; //rad-kolumn, totalt 81 rutor (9x9)
	int testblock[3][3];
	//int input=0;
	int resultatsrad,resultatskolumn;
	FILE *fil;
	fil=fopen("himlen.txt","rt");
	//fil=fopen("himlen2.txt","rt");
	//fil=fopen("himlen3.txt","rt");

	for (int rad1=0; rad1<9; rad1++) {
		for (int kolumn1=0; kolumn1<9; kolumn1++) {
			fscanf(fil,"%d",&block[rad1][kolumn1]);
			//block[rad][kolumn]=input;
			//printf("%d ",block[rad1][kolumn1]);
		}
		//printf("\n");
	}
	fclose(fil);

    int hittad=0;
	for (int rad=0; rad<7; rad++) {
		for (int kolumn=0; kolumn<7; kolumn++) {
			testblock[0][0]=block[rad][kolumn];
			testblock[0][1]=block[rad][kolumn+1];
			testblock[0][2]=block[rad][kolumn+2];
			testblock[1][0]=block[rad+1][kolumn];
			testblock[1][1]=block[rad+1][kolumn+1];
			testblock[1][2]=block[rad+1][kolumn+2];
			testblock[2][0]=block[rad+2][kolumn];
			testblock[2][1]=block[rad+2][kolumn+1];
			testblock[2][2]=block[rad+2][kolumn+2];
            if(testa_blocket(testblock)) { //testblock-matrisen skickas utan index!
                resultatsrad=rad+1;
                resultatskolumn=kolumn+1;
                hittad=1;
                goto R1;
            }
		}
		hittad=0;
	}
    //printf("\n%d %d %d \n%d %d %d \n%d %d %d",testblock[0][0],testblock[0][1],testblock[0][2],testblock[1][0],testblock[1][1],testblock[1][2],testblock[2][0],testblock[2][1],testblock[2][2]);
R1: if (hittad=0) {
        printf("Inga tr�ffar");
    } else {
        printf("Rad %d Kolumn %d",resultatsrad,resultatskolumn);
    }
}
/*
himlen �r 9x9 stor
*/
