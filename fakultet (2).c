#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

int fact(int n) {
    if (n==0) {
        return 1;
    } else {
        return n*fact(n-1);
    }
}

int main(int argc, char* args[]) {
    int n = 0, last_res = 0;
    while (1) {
        int res = fact(n);
        if (res >= INT_MAX || res == 0) {
            //printf("Svar: %d,%d \n",n,res);
            break;
        } else if (res < last_res) {
            printf("\nH\x94gsta m\x94jliga fakultet: %d! = %d \n",n-1,last_res);
            break;
        } else {
            printf("%2d! = %d \n",n,res);
            n++;
            last_res = res;
        }
    }
    //printf("Sista talet: %d",n);
    return 0;
    //printf("Maxv�rde: %d",INT_MAX);
}
