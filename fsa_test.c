//format string attack test
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void funk1() {
    char input[30];
	printf("Skriv in text: ");
	scanf("%s",input);
	//printf("%s",input);
	printf(input);
}

void funk2() {
    printf("hopp!");
}

int main(int argc, char* args[]) {
    funk1();
    //printf("%p",funk2); //adress till funk2: 004012CA

    //BUFFER OVERFLOW
//	sprintf(input,"%s","%*hej");
//	printf(input);
	
	return 0;
}
// BUFFER OVERFLOW: "%*hej"
// resultat:        1.788202e-307j"
// %n
// %x
