//CL071026 Uppgift 7 Bridgehand
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void skrivutkort(int kort) {
    switch (kort) {
        case 1: printf("ERROR"); break;
        case 2: printf("2 "); break;
        case 3: printf("3 "); break;
        case 4: printf("4 "); break;
        case 5: printf("5 "); break;
        case 6: printf("6 "); break;
        case 7: printf("7 "); break;
        case 8: printf("8 "); break;
        case 9: printf("9 "); break;
        case 10: printf("T "); break;
        case 11: printf("J "); break;
        case 12: printf("Q "); break;
        case 13: printf("K "); break;
        case 14: printf("A "); break;
        default: break;
    }
    return;
}

int main(void) {
	//variabler
	int index,i;
	int temp_farg,temp_valor;
	int kort[4][15]={0}; //inneh�ller korten, rader �r f�rg och kolumner val�r.
	FILE *fil;
	fil=fopen("korthand.txt","rt");

	//l�s in kort och sortera
	for (index=0; index<13; index++) {
		fscanf(fil,"%d %d",&temp_farg,&temp_valor);
		kort[temp_farg][temp_valor]=temp_valor;
	}
	fclose(fil);

	printf("Sp| ");
	for (i=14; i>0; i--) {
		skrivutkort(kort[3][i]);
	}
	printf("\nHj| ");
	for (i=14; i>0; i--) {
		skrivutkort(kort[2][i]);
	}
	printf("\nRu| ");
	for (i=14; i>0; i--) {
		skrivutkort(kort[1][i]);
	}
	printf("\nKl| ");
	for (i=14; i>0; i--) {
		skrivutkort(kort[0][i]);
	}
}
