//CL051021 Uppg 2 �kerlappen
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main(void) {
    char mark[20][40];
    int yled_langd=0,xled_langd=0,antal_rader,antal_kolumner;
    FILE *fil;
    fil=fopen("karta.txt","rt");
    //fil=fopen("karta2.txt","rt");

    fscanf(fil,"%d",&antal_rader);
    fscanf(fil,"%d",&antal_kolumner);

    for (int rad=0; rad<antal_rader; rad++) {
    	fscanf(fil,"%s",mark[rad]);
    }

    int markering_kolumn;
    for (int rad=0; rad<antal_rader; rad++) {
        markering_kolumn=1;
    	for (int kolumn=0; kolumn<antal_kolumner; kolumn++) {
    		//i b�rjan av varje rad med o-tecken �kas �kerns l�ngd i y-led med +1
    		if (mark[rad][kolumn]=='o' && markering_kolumn==1) {
    		    yled_langd++;
    		    markering_kolumn=0;
    		}
    	}
    }

    int markering_rad;
    for (int kolumn=0; kolumn<antal_kolumner; kolumn++) {
        markering_rad=1;
    	for (int rad=0; rad<antal_rader; rad++) {
    	    //i b�rjan av varje kolumn med o-tecken �kas �kerns l�ngd i x-led med +1
    		if (mark[rad][kolumn]=='o' && markering_rad==1) {
    		    xled_langd++;
    		    markering_rad=0;
    		}
    	}
    }

    printf("�kerns l�ngd: %d meter \n�kerns bredd: %d meter",xled_langd*10,yled_langd*10);
}
/*
1 ruta �r 10m
*/
