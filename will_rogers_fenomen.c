//CL071026 Uppgift 8 Will Rogers Fenomen
#include <stdio.h>
#include <stdlib.h>

int main(void) {
    //variabler
    int antal_a,antal_b,flytt_tal,fummel,korrektsvar; //antalen ska vara mellan 2-10.
    int a_tal[10],b_tal[10];
    float nytt_medelva=0,nytt_medelvb=0;
    char flytt_grupp; //kan f� v�rdet 'A' eller 'B'.

    //mata in antalet tal i grupp A samt talen.
    printf("Antal tal i grupp A? ");
    scanf("%d",&antal_a);
    if (antal_a<2){
        antal_a=2;
    } else if (antal_a>10) {
        antal_a=10;
    }
    for (int a=0; a<antal_a; a++) {
    	printf("Tal? ");
    	scanf("%d",&a_tal[a]);
    }
    //mata in antalet tal i grupp B samt talen.
    printf("Antal tal i grupp B? ");
    scanf("%d",&antal_b);
    if (antal_b<2){
        antal_b=2;
    } else if (antal_b>10) {
        antal_b=10;
    }
    for (int b=0; b<antal_b; b++) {
    	printf("Tal? ");
    	scanf("%d",&b_tal[b]);
    }

    //ber�kna gruppernas medelv�rden
    float medelv_a=0,summa_a=0;
    for (int c=0; c<antal_a; c++) {
    	summa_a+=a_tal[c];
    }
    medelv_a=summa_a/antal_a;

    float medelv_b=0,summa_b=0;
    for (int d=0; d<antal_b; d++) {
    	summa_b+=b_tal[d];
    }
    medelv_b=summa_b/antal_b;

    //flytta runt tal enl alla kombinationer, titta efter b�da medelv�rdenas �kning
    while (1) {
        for (int e=0; e<=antal_a; e++) {
        //g� igenom alla tal i a_tal, flytta �ver ett till a-summan, ber�kna nya medelv.
        	nytt_medelvb=(a_tal[e]+summa_b)/(antal_b+1); //l�gg till tal fr�n a till b-summan, dela med nya antalet tal i b.
        	nytt_medelva=(summa_a-a_tal[e])/(antal_a-1);
        	if (nytt_medelva>medelv_a && nytt_medelvb>medelv_b) {
                flytt_tal=a_tal[e];
                flytt_grupp='B';
                korrektsvar=1;
                goto L1;
        	}
        }
        for (int e=0; e<=antal_b; e++) {
        //g� igenom alla tal i b_tal, flytta �ver ett till a-summan, ber�kna nya medelv.
        	nytt_medelva=(b_tal[e]+summa_a)/(antal_a+1);
        	nytt_medelvb=(summa_b-b_tal[e])/(antal_b-1);
        	if (nytt_medelva>medelv_a && nytt_medelvb>medelv_b) {
                flytt_tal=b_tal[e];
                flytt_grupp='A';
                korrektsvar=1;
                goto L1;
        	}
        }
        korrektsvar=0;
        goto L1;
    }

    //skriv ut resultatet om det finns, skriv annars felmeddelande
    L1: if (korrektsvar==1) {
        printf("Flytta talet %d till %c",flytt_tal,flytt_grupp);
    } else {
        printf("Om�jligt!");
    }
}
/*
1. L�s in antalet tal i grupp A
2. L�s in lika m�nga tal
3. L�s in antalet tal i grupp B
4. L�s in lika m�nga tal
5. Ber�kna b�da gruppernas medelv�rden
6. Prova vilken enskild siffra som kan flyttas mellan A-B eller B-A f�r att
   b�da gruppernas medelv�rden ska �ka.
   - N�r ett tal flyttats �ver r�knas nya medelv�rden ut, med den skickande gruppens
   n�mnare minskad med -1 och den mottagande gruppens n�mnare �kad med +1.
*/
