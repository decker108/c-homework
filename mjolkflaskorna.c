#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

struct drag{
    int dragnr;
    int flaskor[2];
    int vol[5][3];
    int dragen[20][2];
    struct drag *next;
};

struct drag *start,*stop;
int undersokt=0;

int empty(void){
    return (start==NULL);
}

void enqueue(struct drag d){
    struct drag *ny;
    ny=(struct drag*)malloc(sizeof(struct drag));
    *ny=d;
    if(empty()){
        start=ny;
        start->next=NULL;
    } else {
        stop->next=ny;
    }
    stop=ny;
}

struct drag dequeue(void){
    struct drag *tmp,d;
    d=*start;
    tmp=start;
    start=start->next;
    if(empty()) {
        stop=NULL;
    }
    free(tmp);
    return d;
}

void init(int *nf){
    int i;
    struct drag nu;
    start=NULL;
    printf("Antal flaskor: ");
    scanf("%d",nf);
    for(i=0; i<*nf; i++){
        printf("Flaska %d, volym : ",i+1);
        scanf("%d",&nu.vol[i][0]);
        printf("Flaska %d, inneh�ll: ",i+1);
        scanf("%d",&nu.vol[i][1]);
        printf("Flaska %d, m�l : ",i+1);
        scanf("%d",&nu.vol[i][2]);
    }
    nu.dragnr=0;
    nu.flaskor[0]=-1;
    nu.flaskor[1]=-1;
    enqueue(nu);
}

int koll(int volym[5][3],int nf){
    int i;
    for(i=0;i<nf;i++) {
        if(volym[i][1]!=volym[i][2] && volym[i][2]!=-1) {
            return 0;
        }
    }
    return 1;
}

int main(void){
    int nf,from,to,i,klar;
    struct drag nu,t;
    init(&nf);
    do {
        klar=0;
        if(!empty()) {
            nu=dequeue();
            if (!koll(nu.vol,nf)) {
                for(from=0;from<nf;from++) {
                    for(to=0;to<nf;to++) {
                        if(from!=to){
                        t=nu;
                            if(t.vol[from][1]>0 && t.vol[to][1]<t.vol[to][0] &&
                            !(from==t.flaskor[1] && to==t.flaskor[0])){
                                if(t.vol[to][1]+t.vol[from][1]<=t.vol[to][0]){
                                    t.vol[to][1]=t.vol[to][1]+t.vol[from][1];
                                    t.vol[from][1]=0;
                                }
                                else {
                                    t.vol[from][1]=t.vol[from][1]-
                                    (t.vol[to][0]-t.vol[to][1]);
                                    t.vol[to][1]=t.vol[to][0];
                                }
                                t.dragnr++;
                                t.flaskor[0]=from;
                                t.flaskor[1]=to;
                                t.dragen[t.dragnr][0]=from+1;
                                t.dragen[t.dragnr][1]=to+1;
                                enqueue(t);
                                undersokt++;
                            }
                        }
                    }
                }
            } else {
                klar = 1;
            }
        } else {
            klar = 1;
        }
    } while(!klar);
    printf("Antal unders�kta: %d\n",undersokt);
    for(i=1; i<=nu.dragnr; i++) {
        printf("Drag %2d: Fr�n %d till %d\n", i,nu.dragen[i][0],nu.dragen[i][1]);
    }
    return 0;
}
