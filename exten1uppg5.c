//Exempeltenta 1, Uppgift 5 Virrig vandring
#include <stdio.h>
int ok(int s[][27],int r,int k){
    return s[r+1][k] || s[r-1][k] || s[r][k+1] || s[r][k-1];
}
int main(void){
    int stad[27][27]={0},i,j,rad,kol,dr,dk,steg=0,n,forsok;
    printf("Stadens storlek: ");
    scanf("%d",&n);
    srand(time(0));
    for(forsok=1;forsok<=10000;forsok++){
        steg++;
        for(i=1;i<=n;i++)
            for(j=1;j<=n;j++)
                stad[i][j]=1;
        rad=n/2+1;
        kol=n/2+1;
        stad[rad][kol]=0;
        while(ok(stad,rad,kol)){
            do{
                if(rand()%2){
                    dr=2*(rand()%2)-1;
                    dk=0;
                }
                else {
                    dr=0;
                    dk=2*(rand()%2)-1;
                }
            }while(!stad[rad+dr][kol+dk]);
            steg++;
            rad+=dr; kol+=dk;
            stad[rad][kol]=0;
        }
        stad[rad][kol]=2;
    }
    printf("Vandringens medellängd: %.2f",steg/10000.0);
}
