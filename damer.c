#include <stdio.h>
#include <stdlib.h>

//kolumn[i] anger p� vilken kolumn damen p� i'te raden finns
int kolumn[9]={0,0,0,0,0,0,0,0,0};
//kolfri h�ller reda p� om i'te kolumnen �r ledig. 1 = ledig, 0 = upptagen
int kolfri[9]={1,1,1,1,1,1,1,1,1};
//f�ljande tv� kontrollerar diagonalerna
int uppfri[17]={1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
int nerfri[16]={1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
int antal=0;

void SkrivUt(void){
    int i;
    for (i=1; i<9; i++) {
    	printf("%d ",kolumn[i]);
    }
    printf("\n");
    antal++;
}

//En dam kommer att placeras i varje rad och i varje kolumn.
void AddQueen(int rad){
    int kol;
    rad++;
    for(kol=1;kol<=8;kol++) {
        if(kolfri[kol] && uppfri[rad+kol] && nerfri[rad-kol+8]){
            kolumn[rad]=kol;
            kolfri[kol]=0;
            uppfri[rad+kol]=0;
            nerfri[rad-kol+8]=0;
            if(rad==8) {
                SkrivUt();
            } else {
                AddQueen(rad);
            }
            kolfri[kol]=1;
            uppfri[rad+kol]=1;
            nerfri[rad-kol+8]=1;
        }
    }
    rad--;
}

int main(void){
    AddQueen(0);
    printf("Antal l�sningar %d\n",antal);
    return 0;
}
