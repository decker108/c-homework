#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

int tab[10][10],n,max=0;

void berakna(int rad,int kol,int s){
    if(rad==n-1){
        s=s+tab[rad][kol];
        if(s>max) {
            max=s;
        }
    } else {
        berakna(rad+1,kol,s+tab[rad][kol]);
        berakna(rad+1,kol+1,s+tab[rad][kol]);
    }
}

int main(void){
    FILE *infil;
    int i,j;
    infil=fopen("input.dat","rt");
    fscanf(infil,"%d",&n);
    for(i=0;i<n;i++)
        for(j=0;j<=i;j++)
            fscanf(infil,"%d",&tab[i][j]);
    fclose(infil);
    berakna(0,0,0);
    printf("Maximal summa: %d\n",max);
    return 0;
}
