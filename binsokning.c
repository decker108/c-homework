#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

// wanted: the value wanted from the array
// array[]: the array to be searched
// arrLen: the length of the array
int binsearch(int wanted, int array[], int arrLen){
    int u = 0, n = arrLen - 1, k;
    do {
        k = (u + n) / 2;
        if (wanted < array[k]) {
            n = k - 1;
        } else {
            u = k + 1;
        }
    } while (wanted != array[k] && u <= n);
    if (wanted == array[k]) {
        return k;
    } else {
        return arrLen + 1;
    }
}

int main(void) {
	int array[10] = {1,2,3,4,5,6,7,8,9,10};
	printf("The wanted number is located at: %d", binsearch(2, array, 10));
	return 0;
}
