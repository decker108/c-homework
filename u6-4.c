#include <stdio.h>
#include <stdlib.h>

int stirling(int n, int m) {
    if (n == 0 && m == 0) {
    	return 1;
    }
    if (m == 0) {
    	return 0;
    }
    if (m == 1) {
    	return 1;
    }
    if (n == m) {
    	return 1;
    }
    
    return m * stirling(n-1,m) + stirling(n-1,m-1);
}

int main(void) {
    int n,m;
    while (1) {
		printf("Ange n: ");
		scanf("%d",&n);
		printf("Ange m: ");
		scanf("%d",&m);
		if (n >= m) {
			break;
		} else {
			printf("n m�ste vara st�rre �n eller lika med m, f�rs�k igen! \n");
		}
    }
    printf("{%d \x94ver %d} = %d",n,m,stirling(n,m));
    return 0;
}