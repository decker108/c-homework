#include <stdio.h>
#include <stdlib.h>

int max2(int low, int high) {
    if (low < high) {
        return high;
    } else {
        return low;
    }
}

int maxsekv(int a[], int n) {
    int m = 0;
    int me = 0;
    int i;
    for (i=0; i<n; i++) {
    	me = max2(me+a[i],0); //adderar ihop alla tal i me
    	m = max2(m,me); //kontrollerar om me �r st�rre �n m, isf byts m ut
    }
    return m;
}

int main(void) {
    int n = 10;
	int a[10] = {31,-41,59,26,-53,58,97,-93,-23,84};
	printf("Max-sekvensens summa: %d",maxsekv(a,n));
	return 0;
}
