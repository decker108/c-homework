#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <limits.h>

#define maxSack 20
#define numTypes 7

int size[numTypes] = {2, 3, 2, 3, 6, 1, 2};
int cost[numTypes] = {235, 180, 436, 500, 1000, 250, 480};

//int maxKnapsack(int k, int maxSack, int numTypes, int size[], int cost[]) { /* Knapsack must be filled completely. */
int maxKnapsack(int k) {
    int knapsack[maxSack],max,i,type,total;

    knapsack[0] = 0;
    for (i=1;i<=k;i++) {
        max = -INT_MAX;
        for (type=0;type<numTypes;type++) {
            if (i-size[type]>=0) {
                total = knapsack[i-size[type]]+cost[type];
                if (max<total) {
                    max = total;
                }
            }
        }
        printf("%d %d\n",i,max);
        knapsack[i] = max;
    }
    return knapsack[k];
}

int 

int main(void) {
    int noOfElems, totVol;
    FILE *fil;
    fil = fopen("knapsack.txt","rt");
    fscanf(fil,"%d",&totVol);
    fscanf(fil,"%d",&noOfElems);
    int *size, *cost, j;
    noOfElems++;
    size = (int*) malloc(noOfElems*sizeof(int));
    cost = (int*) malloc(noOfElems*sizeof(int));
    for (j=1; j<noOfElems; j++) {
    	fscanf(fil,"%d %d",&size[j],&cost[j]);
    }

    printf("\nSvar: %d\n",maxKnapsack(20));
    
    return 0;
}
