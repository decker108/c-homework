#include <stdio.h>
#include <assert.h>

#define MAXNUM 5

//char x = 'A';

// a[] = talarrayen, n = maxtalet-1 i permutationen.
// v[] = array som h�ller koll p� anv�nda siffror, p = antal tal som anv�nts i a. 
void solve(int a[],int n,int v[],int p){
    int i,tal;
    if(n == p){ //om ett l�v har n�tts i rekursionstr�det, skriv ut talen
        int sum = 0;
        for (i=0; i<n; i++) {
        	printf("%d ",a[i]);
        }
        printf("\n");
    } else {
        for(tal=0;tal<n;tal++) {
            // OBS: ta bort if-satsen f�r siffer-upprepning
            if (!v[tal]) {  //om talet v[i] ej �r anv�nt...
                v[tal] = 1; //markera tal som anv�nt
                a[p] = tal; //l�gg till talet i permutationsarrayen
                solve(a,n,v,p+1); //g� till n�sta index i arrayen
                v[tal] = 0; //avmarkera tal som oanv�nt
            }
        }
    }
}

int main(void) {
    int a[MAXNUM],v[MAXNUM]={0};
    solve(a,MAXNUM,v,0);
    return 0;
}
