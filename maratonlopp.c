#include <stdio.h>
#include <stdlib.h>

struct roadType {
    int from;
    int to;
    int cost;
};
typedef struct roadType Road;
Road *roads;

int *visitedNodes;
int antal=0,v;

void maraton(int current, int distans, int dest) {
	if (current == dest && distans == 42) {
	    //if (distans == 42) {
	        antal++;
	    //}
	    return;
	} else if (distans > 42) {
	    return;
	} else if (visitedNodes[current] == 1) { //fel?
	    return;
	} else {
	    int i;
	    visitedNodes[current] = 1;
	    for (i=0; i<v; i++) {
	    	if (roads[i].from == current) {
	    	    maraton(roads[i].to,distans+roads[i].cost,dest);
	    	}
	    }
	    visitedNodes[current] = 0;
	}
}

int main(void) {
    int b,dest; // b = antal byar, v = antal v�gar
	FILE *fil;
	fil = fopen("maraton.txt","rt");
	fscanf(fil,"%d",&b);
	fscanf(fil,"%d",&v);
	//skapa struct-array med storlek b
	visitedNodes = (int*) malloc(b*sizeof(int));
	//skapa struct-array med storlek v
    roads = (Road*) malloc(2*v*sizeof(Road)); 
	int i,from,to,cost,k,j;
	for (i=0; i<v; i++) { //l�s in v�gar
		fscanf(fil,"%d %d %d",&from,&to,&cost);
		roads[i].from = from;
		roads[i].to = to;
		roads[i].cost = cost;
	}
	//l�s in v�gar p� nytt, men omv�nt
	rewind(fil);
	fscanf(fil,"%d",&b); //dessa tv� inl�sningar �r f�r
	fscanf(fil,"%d",&v); //att stega fram filpekaren.
	v = 2*v; //eftersom v�gar �r dubbelriktade f�rdubblas v
	for (k=i; k<v; k++) { //l�s in v�gar omv�nt
		fscanf(fil,"%d %d %d",&from,&to,&cost);
		roads[k].from = to;
		roads[k].to = from;
		roads[k].cost = cost;
	}
	
	int a;
	for (a=1; a<=b; a++) {
		antal = 0;
        for (j=0; j<b; j++) {
            visitedNodes[j] = 0;
        }

        //printf("I vilken stad ska loppet starta? ");
        //scanf("%d",&dest);
        dest = a;
        printf("(Vid start i by %2d) ",dest);
        maraton(dest,0,dest);
        printf("Det finns %d olika banor \n",antal);
	}
	
	return 0;
}
