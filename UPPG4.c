//Ola Rende DP09
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct arbetstyp{
    char namn[28];
    float proc[4];
};

int main(void) {
    struct arbetstyp post;
	int val_a,val_b;
	char resultat_namn[28];
    FILE *fil;

	while(1) {
        printf("1. F�rv�rvsarbetande inv�nare \n");
        printf("2. �ppet arbetsl�sa inv \n");
        printf("3. Inv i arbetsmarknprogr \n");
        printf("4. L�ngstidsarbtsl�sa inv \n");
        scanf("%d",&val_a);
        if(val_a==1 || val_a==2 || val_a==3 || val_a==4) {
            break;
        }
	}
	while (1) {
		printf("1. St�rsta andelen\n");
		printf("2. Minsta andelen\n");
		scanf("%d",&val_b);
		if(val_b==1 || val_b==2) {
            break;
        }
	}

    float max=0.0,min=100.0;
    fil=fopen("arbete.txt","rt");
    //fil=fopen("arbete.dat","rb");
    while(!feof(fil)) {
    //for (int a=0; a<10; a++) {
        fread(&post,sizeof(struct arbetstyp),1,fil);
        //kolla efter st�rsta eller minsta procenttalet
        //printf("%s: %f %f %f %f\n",post.namn,post.proc[0],post.proc[1],post.proc[2],post.proc[3]);
        if (val_b==1) { //max-s�kning
            if (val_a==1){
                if(post.proc[0]>max) {
                    max=post.proc[0];
                    strcpy(resultat_namn,post.namn);
                }
            }
            else if (val_a==2){
                if(post.proc[1]>max) {
                    max=post.proc[1];
                    strcpy(resultat_namn,post.namn);
                }
            }
            else if (val_a==3){
                if(post.proc[2]>max) {
                    max=post.proc[2];
                    strcpy(resultat_namn,post.namn);
                }
            }
            else { //annars �r val_a=4
                if(post.proc[3]>max) {
                    max=post.proc[3];
                    strcpy(resultat_namn,post.namn);
                }
            }
        } else { //annars �r val_b==2 (min-s�kning)
            if (val_a==1){
                if(post.proc[0]<min){
                    min=post.proc[0];
                    strcpy(resultat_namn,post.namn);
                }
            }
            else if (val_a==2){
                if(post.proc[1]<min){
                    min=post.proc[1];
                    strcpy(resultat_namn,post.namn);
                }
            }
            else if (val_a==3){
                if(post.proc[2]<min){
                    min=post.proc[2];
                    strcpy(resultat_namn,post.namn);
                }
            }
            else { //annars �r val_a=4
                if(post.proc[3]<min){
                    min=post.proc[3];
                    strcpy(resultat_namn,post.namn);
                }
            }
        }
    }
    if (val_b==1) {
        printf("%s %.1f%%",resultat_namn,max);
    } else {
        printf("%s %.1f%%",resultat_namn,min);
    }
    fclose(fil);
}
