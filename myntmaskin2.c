#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

int min_coins = INT_MAX;
int target = 0;

void insert_coin(int score, int coins) {
    if (score == target) {
        if (coins < min_coins) {
            min_coins = coins;
        }
    } else {
        if (score*3 == target || score*3 <= target-4) {
            insert_coin(score*3,coins+10);
        }
        if (score+4 <= target) {
            insert_coin(score+4,coins+5);
        }
    }
}

int main(int argc, char* args[]) {
    printf("Vilken po\x84ng som ska uppn\x86s: ");
    scanf("%d",&target);
	insert_coin(1,0); //1p, 0 coins
	printf("Po\x84ngen kan n\x86s med %d \x94re",min_coins);
	return 0;
}