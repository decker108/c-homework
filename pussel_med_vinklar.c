#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

#define ABLOCK 1
#define BBLOCK 2
#define CBLOCK 3
#define DBLOCK 4

int antal = 0;

int test(int board[5][5], int blockType, int k, int m) {
    if (blockType == ABLOCK) {
        if (k+1 < 5 && m+1 < 5) {
            if (board[k][m+1] == 0 && board[k+1][m] == 0 && board[k+1][m+1] == 0) {
                return 1;
            }
        }
    } else if (blockType == BBLOCK) {
        if (k+1 < 5 && m+1 < 5) {   
            if (board[k][m] == 0 && board[k+1][m] == 0 && board[k+1][m+1] == 0) {
                return 1;
            }
        }
    } else if (blockType == CBLOCK) {
        if (k+1 < 5 && m+1 < 5) {   
            if (board[k][m] == 0 && board[k][m+1] == 0 && board[k+1][m+1] == 0) {
                return 1;
            }
        }
    } else if (blockType == DBLOCK) {
        if (k+1 < 5 && m+1 < 5) {   
            if (board[k][m] == 0 && board[k][m+1] == 0 && board[k+1][m] == 0) {
                return 1;
            }
        }
    }
    return 0;
}

int solved(int board[5][5]) {
    int i,j;
    for (i = 0; i < 5; i++) {
    	for (j = 0; j < 5; j++) {
    		if (board[i][j] == 0) {
    		    return 0;
    		}
    	}
    }
    return 1;
}

void puzzle(int board[5][5], int blocks) {
    if (blocks > 8) {
        return;
    }
    if (solved(board)) {
        printf("Solution found! Blocks: %d \n",blocks);
        antal++;
        int i,j;
        for (i=0; i<5; i++) {
        	for (j=0; j<5; j++) {
        		printf("%d ",board[i][j]);
        	}
        	printf("\n");
        }
        //return;
        exit(0);
    } else {
        int k,m;
        for (k=0; k<5; k++) {
        	for (m=0; m<5; m++) {
                // prova bit A
                if (test(board,ABLOCK,k,m)) {
                //if (board[k][m+1] == 0 && board[k+1][m] == 0 && board[k+1][m+1] == 0) {
                    board[k][m+1]   = 1;
                    board[k+1][m]   = 1;
                    board[k+1][m+1] = 1;
                    puzzle(board,blocks+1);
                    board[k][m+1]   = 0;
                    board[k+1][m]   = 0;
                    board[k+1][m+1] = 0;
                }
                // prova bit B
                //if (board[k][m] == 0 && board[k+1][m] == 0 && board[k+1][m+1] == 0) {
                if (test(board,BBLOCK,k,m)) {
                    board[k][m]     = 2;
                    board[k+1][m]   = 2;
                    board[k+1][m+1] = 2;
                    puzzle(board,blocks+1);
                    board[k][m]     = 0;
                    board[k+1][m]   = 0;
                    board[k+1][m+1] = 0;
                }
                // prova bit C
                if (test(board,CBLOCK,k,m)) {
                //if (board[k][m] == 0 && board[k][m+1] == 0 && board[k+1][m+1] == 0) {
                    board[k][m]     = 3;
                    board[k][m+1]   = 3;
                    board[k+1][m+1] = 3;
                    puzzle(board,blocks+1);
                    board[k][m]     = 0;
                    board[k][m+1]   = 0;
                    board[k+1][m+1] = 0;
                }
                // prova bit D
                if (test(board,DBLOCK,k,m)) {
                //if (board[k][m] == 0 && board[k][m+1] == 0 && board[k+1][m] == 0) {
                    board[k][m]   = 4;
                    board[k][m+1] = 4;
                    board[k+1][m] = 4;
                    puzzle(board,blocks+1);
                    board[k][m]   = 0;
                    board[k][m+1] = 0;
                    board[k+1][m] = 0;
                }
        	}
        }
    }
}

int main(void) {
    int board[5][5];
    int i,j;
    for (i=0; i<5; i++) {
        for (j=0; j<5; j++) {
            board[i][j] = 0;
        }
    }
	board[0][2] = 1;
	puzzle(board,0);
	printf("Antal losningar: %d",antal);
	return 0;
}
