//CL040413 Uppg 4 Flygbokning
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct flygtur{
    char flightnr[8];
    char fran[12];
    char till[12];
    int tim,min,antal;
};

int main(void) {
	//variabler
	struct flygtur resa;
	char till_input[12];
	char fran_input[12];
	int antal_input,n=0;
	FILE *fil;
	fil=fopen("bok.dat","rb");

	printf("Fr�n? ");
	scanf("%s",fran_input);
	printf("Till? ");
	scanf("%s",till_input);
    printf("Antal resande? ");
    scanf("%d",&antal_input);

	while(!(feof(fil))){
	    fread(&resa,sizeof(struct flygtur),1,fil);
	    int jmf_fran=strcmp(fran_input,resa.fran);
	    int jmf_till=strcmp(till_input,resa.till);
	    if (resa.antal>=antal_input && jmf_till==0 && jmf_fran==0) {
	    	printf("%s %d:%d \n",resa.flightnr,resa.tim,resa.min);
	    }
	}

	//resultats-utskrift
	//printf("%d",n);
}
/*
Antal flights? 59
*/
